#include "disasm.h"

#include "memory.h"
#include <sstream>

std::string Disasm::do_disasm(uint16 address, uint count)
{
    std::stringstream stream;
    //std::string str;
    BYTE command;
    uint sadr;
    for(uint i=0;i<count;i++){
        sadr = address;
        if(!Memory::read(address,command)){
            stream<<std::hex<<address<<" : "<<"[ERR]"<<std::endl;
            address++;
            continue;
        }

        BYTE group =  command & 0b00000011;
        BYTE code  = (command & 0b11100000) >> 5;
        BYTE code2 = (command & 0b00011100) >> 2;

        //bool found=false;

        if(group==0){
            if(code==0){
                if(code2==4){
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    address++;
                    int line = address;
                    line+=(char)op;

                    stream<<std::hex<<sadr<<" : "<<"BPL 0x"<<line<<" ;(0x"<<(uint)op<<")"<<std::endl;
                    continue;
                }
                if(code2==6){
                    //CLC
                    address++;
                    stream<<std::hex<<sadr<<" : "<<"CLC"<<std::endl;
                    continue;
                }
            }
            if(code==1){
                if(code2==0){
                    address++;
                    uint16 a;
                    Memory::read(address,a);
                    address+=2;
                    stream<<std::hex<<sadr<<" : "<<"JSR 0x"<<(uint)a<<std::endl;
                    continue;
                }
                if(code2==3){
                    //BIT OP (ABS)
                    address++;
                    uint16 a;
                    Memory::read(address,a);
                    address+=2;
                    stream<<std::hex<<sadr<<" : "<<"BIT 0x"<<(uint)a<<std::endl;
                    continue;
                }
                if(code2==4){
                    //BMI Op
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    address++;
                    int line = address;
                    line+=(char)op;
                    stream<<std::hex<<sadr<<" : "<<"BMI 0x"<<(uint)line<<std::endl;
                    continue;
                }
            }
            if(code==2){
                if(code2==3){
                    address++;
                    uint16 a;
                    Memory::read(address,a);
                    address+=2;
                    stream<<std::hex<<sadr<<" : "<<"JMP 0x"<<(uint)a<<std::endl;
                    continue;
                }
                if(code2==4){
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    address++;
                    int line = address;
                    line+=(char)op;

                    stream<<std::hex<<sadr<<" : "<<"BVC 0x"<<line<<" ;(0x"<<(uint)op<<")"<<std::endl;
                    continue;
                }
            }
            if(code==3){
                if(code2==0){
                    //RTS
                    stream<<std::hex<<sadr<<" : "<<"RTS"<<std::endl;
                    address++;
                    continue;
                }
                if(code2==6){
                    stream<<std::hex<<sadr<<" : "<<"SEI"<<std::endl;
                    address++;
                    continue;
                }
            }
            if(code==4){
                if(code2==1){
                    //STY (ZERO PAGE)
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"STY 0x"<<std::hex<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
                if(code2==2){
                    //DEY
                    stream<<std::hex<<sadr<<" : "<<"DEY"<<std::endl;
                    address++;
                    continue;
                }
                if(code2==3){
                    //STY Oper(ABS)
                    address++;
                    uint16 op;
                    Memory::read(address,op);
                    address+=2;
                    stream<<std::hex<<sadr<<" : "<<"STY 0x"<<std::hex<<(uint)op<<std::endl;
                    continue;
                }
                if(code2==4){
                    //BCC Oper
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    address++;
                    int line = address;
                    line+=(char)op;

                    stream<<std::hex<<sadr<<" : "<<"BCC 0x"<<line<<" ;(0x"<<(uint)op<<")"<<std::endl;
                    continue;
                }
                if(code2==6){
                    //TYA
                    stream<<std::hex<<sadr<<" : "<<"TYA"<<std::endl;
                    address++;
                    continue;
                }
            }
            if(code==5){
                if(code2==0){
                    //LDY #Oper
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"LDY #0x"<<std::hex<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
                if(code2==1){
                    //LDY Oper (ZP)
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"LDY 0x"<<std::hex<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
                if(code2==2){
                    //TAY
                    stream<<std::hex<<sadr<<" : "<<"TAY"<<std::endl;
                    address++;
                    continue;
                }
            }
            if(code==6){
                if(code2==0){
                    //CPY *Oper
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"CPY *0x"<<std::hex<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
                if(code2==2){
                    //INY
                    stream<<std::hex<<sadr<<" : "<<"INY"<<std::endl;
                    address++;
                    continue;
                }
                if(code2==4){
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    address++;
                    int line = address;
                    line+=(char)op;

                    stream<<std::hex<<sadr<<" : "<<"BNE 0x"<<line<<" ;(0x"<<(uint)op<<")"<<std::endl;
                    continue;
                }
                if(code2==6){
                    stream<<std::hex<<sadr<<" : "<<"CLD"<<std::endl;
                    address++;
                    continue;
                }
            }
            if(code==7){
                if(code2==0){
                    BYTE op;
                    address++;
                    Memory::read(address,op);
                    address++;
                    stream<<std::hex<<sadr<<" : "<<"CPX *0x"<<(uint)op<<std::endl;
                    continue;
                }
                if(code2==2){
                    stream<<std::hex<<sadr<<" : "<<"INX"<<std::endl;
                    address++;
                    continue;
                }
                if(code2==6){
                    stream<<std::hex<<sadr<<" : "<<"SED"<<std::endl;
                    address++;
                    continue;
                }
            }
        }
        if(group==1){
            if(code==1){
                if(code2==2){
                    //AND #Oper
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    address++;
                    stream<<std::hex<<sadr<<" : "<<"AND #0x"<<std::hex<<(uint)op<<std::endl;
                    continue;
                }
            }
            if(code==2){
                if(code2==2){
                    //EOR #Oper
                    BYTE op;
                    address++;
                    Memory::read(address,op);
                    address++;
                    stream<<std::hex<<sadr<<" : "<<"EOR #0x"<<std::hex<<(uint)op<<std::endl;
                    continue;
                }
                if(code2==6){
                    //EOR Oper,Y
                    address++;
                    uint16 addr;
                    Memory::read(address,addr);
                    address+=2;
                    stream<<std::hex<<sadr<<" : "<<"EOR 0x"<<std::hex<<addr<<", Y"<<std::endl;
                    continue;
                }
            }
            if(code==3){
                if(code2==1){
                    BYTE op;
                    address++;
                    op=Memory::read(address,op);
                    address++;
                    stream<<std::hex<<sadr<<" : "<<"ADC 0x"<<std::hex<<(uint)op<<std::endl;
                    continue;
                }
                if(code2==2){
                    BYTE op;
                    address++;
                    op=Memory::read(address,op);
                    address++;
                    stream<<std::hex<<sadr<<" : "<<"ADC #0x"<<std::hex<<(uint)op<<std::endl;
                    continue;
                }
            }
            if(code==4){
                if(code2==1){
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    address++;
                    stream<<std::hex<<sadr<<" : "<<"STA 0x"<<std::hex<<(uint)op<<std::endl;
                    continue;
                }
                if(code2==3){
                    address++;
                    uint16 addr;
                    Memory::read(address,addr);
                    stream<<std::hex<<sadr<<" : "<<"STA 0x"<<std::hex<<(uint)addr<<std::endl;
                    address+=2;
                    continue;
                }
                if(code2==4){
                    //STA (Oper),Y
                    BYTE op;
                    address++;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"STA (0x"<<std::hex<<(uint)op<<"),Y"<<std::endl;
                    address++;
                    continue;
                }
                if(code2==5){
                    //STA Oper,X (ZP)
                    BYTE adr;
                    address++;
                    Memory::read(address,adr);
                    stream<<std::hex<<sadr<<" : "<<"STA 0x"<<std::hex<<(uint)adr<<",X"<<std::endl;
                    address++;
                    continue;
                }
                if(code2==6){
                    //STA Oper,Y
                    uint16 adr;
                    address++;
                    Memory::read(address,adr);
                    stream<<std::hex<<sadr<<" : "<<"STA 0x"<<std::hex<<(uint)adr<<",X"<<std::endl;
                    address+=2;
                    continue;
                }
                if(code2==7){
                    //STA Oper,X
                    address++;
                    uint16 addr;
                    Memory::read(address,addr);
                    stream<<std::hex<<sadr<<" : "<<"STA 0x"<<std::hex<<(uint)addr<<",X"<<std::endl;
                    address+=2;
                    continue;
                }
            }
            if(code==5){
                if(code2==1){
                    //LDA Oper ;(ZP)
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"LDA 0x"<<std::hex<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
                if(code2==2){
                    //LDA #Oper
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"LDA #0x"<<std::hex<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
                if(code2==3){
                    uint16 addr;
                    address++;
                    Memory::read(address,addr);
                    stream<<std::hex<<sadr<<" : "<<"LDA 0x"<<std::hex<<(uint)addr<<std::endl;
                    address+=2;
                    continue;
                }
                if(code2==4){
                    //LDA (Oper),Y
                    BYTE op;
                    address++;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"LDA (0x"<<std::hex<<(uint)op<<"),Y"<<std::endl;
                    address++;
                    continue;
                }
                if(code2==7){
                    //LDA Oper,X
                    uint16 addr;
                    address++;
                    Memory::read(address,addr);
                    stream<<std::hex<<sadr<<" : "<<"LDA 0x"<<std::hex<<(uint)addr<<",X"<<std::endl;
                    address+=2;
                    continue;
                }
            }
            if(code==6){
                if(code2==2){
                    //CMP #Oper
                    BYTE op;
                    address++;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"CMP #0x"<<std::hex<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
                if(code2==7){
                    //CMP Oper,X ;(Absolute)
                    uint16 addr;
                    address++;
                    Memory::read(address,addr);
                    stream<<std::hex<<sadr<<" : "<<"CMP 0x"<<std::hex<<(uint)addr<<",X"<<std::endl;
                    address+=2;
                    continue;
                }
            }
        }
        if(group==2){
            if(code==0){
                if(code2==2){
                    //ASL A
                    stream<<std::hex<<sadr<<" : "<<"ASL A"<<std::endl;
                    address++;
                    continue;
                }
            }
            if(code==4){
                if(code2==1){
                    //STX
                    BYTE op;
                    address++;
                    Memory::read(address,op);
                    address++;
                    stream<<std::hex<<sadr<<" : "<<"STX 0x"<<(uint)op<<std::endl;
                    continue;
                }
                if(code2==2){
                    //TXA
                    stream<<std::hex<<sadr<<" : "<<"TXA"<<std::endl;
                    address++;
                    continue;
                }
                if(code2==3){
                    //STX Oper (ABS)
                    uint16 op;
                    address++;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"STX 0x"<<op<<std::endl;
                    address+=2;
                    continue;
                }
                if(code2==6){
                    //TXS
                    stream<<std::hex<<sadr<<" : "<<"TXS"<<std::endl;
                    address++;
                    continue;
                }
            }
            if(code==5){
                if(code2==0){
                    //LDX #Oper
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"LDX #0x"<<std::hex<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
                if(code2==1){
                    //LDX Oper (ZP)
                    address++;
                    BYTE op;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"LDX 0x"<<std::hex<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
                if(code2==2){
                    //TAX
                    stream<<std::hex<<sadr<<" : "<<"TAX"<<std::endl;
                    address++;
                    continue;
                }
            }
            if(code==6){
                if(code2==2){
                    //DEX
                    stream<<std::hex<<sadr<<" : "<<"DEX"<<std::endl;
                    address++;
                    continue;
                }
            }
            if(code==7){
                if(code2==1){
                    //INC
                    BYTE op;
                    address++;
                    Memory::read(address,op);
                    stream<<std::hex<<sadr<<" : "<<"INC 0x"<<(uint)op<<std::endl;
                    address++;
                    continue;
                }
            }
        }




        stream<<std::hex<<address<<" : "<<"["<<((uint)command)<<"](?)(group:"<<(uint)group<<")"<<"(code:"<<(uint)code<<")"<<"(code2:"<<(uint)code2<<")"<<std::endl;
        address++;




    }
    return stream.str();
}
