#ifndef APU_H
#define APU_H

#include "defines.h"

class APU
{
public:
    static void access(uint address, uint size , uint mode, uint &value);
};

#endif // APU_H
