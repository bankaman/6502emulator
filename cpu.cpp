#include "cpu.h"
#include "memory.h"

uint16 CPU::PC = 0;
BYTE CPU::A = 0;
BYTE CPU::X = 0;
BYTE CPU::Y = 0;
BYTE CPU::S = 0;
BYTE CPU::P = 0b00100000;

void CPU::reset()
{
    A = 0;
    X = 0;
    Y = 0;
    S = 0;
    P = 0b00100000;
    Memory::read(0xFFFC,PC);
    std_debug("PC:",std::hex,PC);
}

bool CPU::step()
{
    BYTE command;
    uint16 operand;
    BYTE operand8;
    Memory::read(PC,command);

    BYTE group =  command & 0b00000011;
    BYTE code  = (command & 0b11100000) >> 5;
    BYTE code2 = (command & 0b00011100) >> 2;

    if(group==0){
        if(code==0){
            if(code2==4){ //BPL Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                PC+=2;
                if((!is_negative()) && (!is_zero())){
                    PC+=(char)operand8;
                }
                return true;
            }
            if(code2==6){ //CLC
                clear_C();
                PC++;
                return true;
            }
        }
        if(code==1){
            if(code2==0){ //JSR Oper
                if(!Memory::read(PC+1,operand))
                    return false;
                if(!Memory::write(S+0x100-2,(uint16)(PC+2)))
                    return false;
                S-=2;
                PC=operand;
                return true;
            }
            if(code2==3){
                //BIT OP (absolute)
                if(!Memory::read(PC+1,operand))
                    return false;
                if(!Memory::read(operand,operand8))
                    return false;

                update_ZN_Flags(operand8 & A);
                P=(operand8 & 0b11000000) | (0b00111111&P);
                PC+=3;
                return true;
            }
            if(code2==4){
                //BMI Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(is_negative()){
                    PC=PC+2+((char)operand8);
                }else{
                    PC+=2;
                }
                return true;
            }
        }
        if(code==2){
            if(code2==3){
                //JMP (Absolute)
                if(!Memory::read(PC+1,PC))
                    return false;
                return true;
            }
        }
        if(code==3){
            if(code2==0){ //RTS
                if(!Memory::read(S+0x100,operand))
                    return false;
                S+=2;
                PC=operand+1;
                return true;
            }
            if(code2==6){ //SEI
                P|=0b00000100;
                PC++;
                return true;
            }
        }
        if(code==4){
            if(code2==1){
                //STY Oper (ZP)
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(!Memory::write(operand8,Y))
                    return false;
                PC+=2;
                return true;
            }
            if(code2==2){
                //DEY
                Y--;
                PC++;
                update_ZN_Flags(Y);
                return true;
            }
            if(code2==3){
                //STY Oper (ABS)
                if(!Memory::read(PC+1,operand))
                    return false;
                if(!Memory::write(operand,Y))
                    return false;
                PC+=3;
                return true;
            }
            if(code2==4){
                //BCC Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                PC+=2;
                if(!is_C_set()){
                    PC+=(char)operand8;
                }
                return true;
            }
            if(code2==6){
                //TYA
                A=Y;
                update_ZN_Flags(A);
                PC++;
                return true;
            }
        }
        if(code==5){
            if(code2==0){ //LDY #Oper
                if(!Memory::read(PC+1,Y))
                    return false;
                PC+=2;
                update_ZN_Flags(Y);
                return true;
            }
            if(code2==1){
                //LDY Oper ;(ZP)
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(!Memory::read(operand8,Y))
                    return false;
                update_ZN_Flags(Y);
                PC+=2;
                return true;
            }
            if(code2==2){
                //TAY
                Y=A;
                update_ZN_Flags(Y);
                PC++;
                return true;
            }
        }
        if(code==6){
            if(code2==0){
                //CPY *Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                operand=Y-operand8;
                update_ZN_Flags(operand);
                if(Y>=operand8)
                    set_C();
                else
                    clear_C();
                PC+=2;
                return true;
            }
            if(code2==2){
                //INY
                Y++;
                update_ZN_Flags(Y);
                PC++;
                return true;
            }
            if(code2==4){ //BNE Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                PC+=2;
                if(!is_zero()){
                    PC+=(char)operand8;
                }
                return true;
            }
            if(code2==6){ //CLD
                P&=0b11110111;
                PC++;
                return true;
            }
        }
        if(code==7){
            if(code2==0){ //CPX *Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(X>=operand8){
                    set_C();
                }else{
                    clear_C();
                }
                operand8=X-operand8;
                update_ZN_Flags(operand8);
                PC+=2;
                return true;
            }
            if(code2==2){ //INX
                X++;
                update_ZN_Flags(X);
                PC++;
                return true;
            }
            if(code2==6){ //SED
                set_D();
                PC++;
                return true;
            }
        }
    }
    if(group==1){
        if(code==1){
            if(code2==2){
                //AND #Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                A=A&operand8;
                update_ZN_Flags(A);
                PC+=2;
                return true;
            }
        }
        if(code==2){
            if(code2==2){ //EOR #Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                A^=operand8;
                PC+=2;
                update_ZN_Flags(A);
                return true;
            }
        }
        if(code==3){
            if(code2==1){ //ADC Oper (ZP)
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(!Memory::read(operand8,operand8))
                    return false;
                operand=A+operand8;
                A=operand&(0xFF);
                update_ZN_Flags(A);
                if((operand&0b100000000) > 0){
                    set_C();
                }else{
                    clear_C();
                }
                if((operand&0b010000000) > 0){
                    set_V();
                }else{
                    clear_V();
                }
                PC+=2;
                return true;
            }
            if(code2==2){ //ADC #Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                operand=A+operand8;
                A=operand&(0xFF);
                update_ZN_Flags(A);
                if((operand&0b100000000) > 0){
                    set_C();
                }else{
                    clear_C();
                }
                if((operand&0b010000000) > 0){
                    set_V();
                }else{
                    clear_V();
                }
                PC+=2;
                return true;
            }
        }
        if(code==4){
            if(code2==1){ //STA Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(!Memory::write(operand8,A))
                    return false;
                PC+=2;
                return true;
            }
            if(code2==3){ //STA Oper
                if(!Memory::read(PC+1,operand))
                    return false;
                if(!Memory::write(operand,A))
                    return false;
                PC+=3;
                return true;
            }
            if(code2==4){
                //STA (Oper),Y
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(!Memory::read(operand8,operand))
                    return false;
                operand+=Y;
                if(!Memory::write(operand,A))
                    return false;
                PC+=2;
                return true;
            }
            if(code2==5){ //STA Oper,X (ZP)
                if(!Memory::read(PC+1,operand8))
                    return false;
                operand8+=X;
                if(!Memory::write(operand8,A))
                    return false;
                PC+=2;
                return true;
            }
            if(code2==6){ //STA Oper,Y (ABS)
                if(!Memory::read(PC+1,operand))
                    return false;
                operand+=Y;
                if(!Memory::write(operand,A))
                    return false;
                PC+=3;
                return true;
            }
            if(code2==7){ //STA Oper,X
                if(!Memory::read(PC+1,operand))
                    return false;
                operand+=X;
                if(!Memory::write(operand,A))
                    return false;
                PC+=3;
                return true;
            }
        }
        if(code==5){
            if(code2==1){ //LDA Oper (ZP)
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(!Memory::read(operand8,A))
                    return false;
                PC+=2;
                update_ZN_Flags(A);
                return true;
            }
            if(code2==2){ //LDA #Oper
                if(!Memory::read(PC+1,A))
                    return false;
                PC+=2;
                update_ZN_Flags(A);
                return true;
            }
            if(code2==3){ //LDA Oper
                if(!Memory::read(PC+1,operand))
                    return false;

                if(!Memory::read(operand,A))
                    return false;

                update_ZN_Flags(A);
                PC+=3;
                return true;
            }
            if(code2==4){
                //LDA (Oper),Y
                if(!Memory::read(PC+1,operand8))
                    return false;
                BYTE t;
                if(!Memory::read(operand8,t))
                    return false;
                operand=t;
                if(!Memory::read(operand8+1,t))
                    return false;
                operand |= (t<<8);
                operand += Y;
                ndebug("LDA address: "+to_hex(operand));
                if(!Memory::read(operand,A))
                    return false;
                update_ZN_Flags(A);
                PC+=2;
                return true;
            }
            if(code2==7){ //LDA Oper,X
                if(!Memory::read(PC+1,operand))
                    return false;
                operand+=X;
                if(!Memory::read(operand,A))
                    return false;
                update_ZN_Flags(A);
                PC+=3;
                return true;
            }
        }
        if(code==6){
            if(code2==2){
                //CMP #Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(A>=operand8){
                    set_C();
                }else{
                    clear_C();
                }
                operand8=A-operand8;
                update_ZN_Flags(operand8);
                PC+=2;
                return true;
            }
            if(code2==7){
                //CMP Oper,X (Abs)
                if(!Memory::read(PC+1,operand))
                    return false;
                operand+=X;
                if(!Memory::read(operand,operand8))
                    return false;
                if(X>=operand8){
                    set_C();
                }else{
                    clear_C();
                }
                operand8=X-operand8;
                update_ZN_Flags(operand8);
                PC+=3;
                return true;
            }
        }
    }
    if(group==2){
        if(code==0){
            if(code2==2){ //ASL A
                if((A&0b10000000)>0)
                    set_C();
                else
                    clear_C();
                A<<=1;
                update_ZN_Flags(A);
                PC++;
                return true;
            }
        }
        if(code==4){
            if(code2==1){ //STX Op (ZP)
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(!Memory::write(operand8,X))
                    return false;
                PC+=2;
                return true;
            }
            if(code2==2){ //TXA
                A=X;
                PC++;
                update_ZN_Flags(A);
                return true;
            }
            if(code2==3){
                //STX Oper (ABS)
                if(!Memory::read(PC+1,operand))
                    return false;
                if(!Memory::write(operand,X))
                    return false;
                PC+=3;
                return true;
            }
            if(code2==6){ //TXS
                PC++;
                S=X;
                return true;
            }
        }
        if(code==5){
            if(code2==0){ //LDX #Oper
                if(!Memory::read(PC+1,X))
                    return false;
                PC+=2;
                update_ZN_Flags(X);
                return true;
            }
            if(code2==1){ //LDX Oper (ZP)
                if(!Memory::read(PC+1,operand8))
                    return false;
                if(!Memory::read(operand8,X))
                    return false;
                PC+=2;
                update_ZN_Flags(X);
                return true;
            }
            if(code2==2){ //TAX
                X=A;
                PC++;
                update_ZN_Flags(X);
                return true;
            }
        }
        if(code==6){
            if(code2==2){
                //DEX
                PC++;
                X--;
                update_ZN_Flags(X);
                return true;
            }
        }
        if(code==7){
            if(code2==1){
                //INC Oper
                if(!Memory::read(PC+1,operand8))
                    return false;
                operand=(uint16)operand8;
                if(!Memory::read(operand,operand8))
                    return false;
                operand8++;
                if(!Memory::write(operand,operand8))
                    return false;
                update_ZN_Flags(operand8);
                PC+=2;
                return true;
            }
        }
    }

    ndebug("CPU: Undefined command at 0x"+to_hex(PC)+"("+to_hex(command)+")"+"| group:"+std::to_string(group)+" code:"+std::to_string(code)+" code2:"+std::to_string(code2)+"\n");
    return false;
}

void CPU::update_ZN_Flags(unsigned char value)
{
    P&=0b01111111;
    P|=(value&0b10000000);
    P&=0b11111101;
    P|= (value==0)?0b10:0;
}

void CPU::clear_C()
{
    P&=0b11111110;
}

void CPU::clear_D()
{
    P&=0b11110111;
}

void CPU::set_C()
{
    clear_C();
    P|=0b00000001;
}

void CPU::set_D()
{
    clear_D();
    P|=0b00001000;
}

void CPU::set_V()
{
    clear_V();
    P|=0b01000000;
}

void CPU::clear_V()
{
    P&=0b10111111;
}

bool CPU::is_zero()
{
    return ((P&0b00000010)>0);
}

bool CPU::is_negative()
{
    return ((P&0b10000000)>0);
}

bool CPU::is_C_set()
{
    return ((P&0b00000001)>0);
}
