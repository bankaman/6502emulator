#include "ppu.h"

#include "ppumemory.h"

BYTE PPU::PPUCTRL=0;
BYTE PPU::PPUSTATUS=0;
BYTE PPU::PPUMASK=0;
uint16 PPU::PPUADDR=0;
bool PPU::ppuaddr_h=true;

BYTE PPU::SCROLL_X = 0;
BYTE PPU::SCROLL_Y = 0;
bool PPU::SCROLL_write_flag = true;

BYTE PPU::OAM_ADDR = 0;

//ByteArray PPU::memory = ByteArray(0x4000);

void PPU::access(uint address, uint size, uint mode, uint &value)
{
    //PPUCTRL
    if(mode==Memory::MODE_WRITE){
        if(address==0x2000){
            PPUCTRL = value;
        }
        if(address==0x2001){
            PPUMASK = value;
        }
        if(address>=0x2002 && address<=0x2004 && address!=0x2003){
            ndebug("Writing to "+to_hex(address)+" not implemented!\n");
            return;
        }
        if(address==0x2003){
            OAM_ADDR=value;
        }
        if(address==0x2005){
            if(SCROLL_write_flag)
                SCROLL_X = value;
            else
                SCROLL_Y = value;
            SCROLL_write_flag=!SCROLL_write_flag;
        }
        if(address==0x2006){
            if(ppuaddr_h){
                //ndebug("write to 0x2006! val:"+to_hex(value)+"\n");
                PPUADDR=0;
                PPUADDR|= (uint16)value << 8;
                ppuaddr_h=false;
                //ndebug("addr:"+to_hex(PPUADDR)+"\n");
            }else{
                ppuaddr_h=true;
                PPUADDR|= value;
            }
        }
        if(address==0x2007){
            PPUMemory::write(PPUADDR,(BYTE)value);
            PPUADDR++;
        }
        //PPUSTATUS |= (0b00010111 & value);
    }else{
        if(address==0x2002){
            value=PPUSTATUS;
        }
        if((address>=0x2000 && address<=0x2001) || (address>=0x2003 && address<=0x2007)){
            ndebug("Reading from "+to_hex(address)+" not implemented!\n");
            return;
        }
    }
}

bool PPU::memory_read(unsigned short address, unsigned char &value)
{
    if(address>0x3fff)
        return false;

    return PPUMemory::read(address,value);
}

unsigned short PPU::get_tiles_address()
{
    uint16 t =((PPUCTRL&0b10000)>0) ? 0x1000 : 0;
    return t;
}
