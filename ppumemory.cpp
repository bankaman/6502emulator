#include "ppumemory.h"

std::vector<Memory::region*> PPUMemory::space;


bool PPUMemory::write(unsigned int address, unsigned char size, unsigned int value)
{
    Memory::region* r = get_region(address);

    if(r==nullptr){
        ndebug("ERROR VIDEO: Writing unmapped memory: 0x"+to_hex(address)+"\n");
        return false;
    }

    if((r->mode & Memory::MODE_WRITE) == 0){
        ndebug("ERROR VIDEO: Writing read-only memory: 0x"+to_hex(address)+"\n");
        return false;
    }

    switch (r->type) {
    case Memory::TYPE_CALLBACK:
        r->callback(address,size,Memory::MODE_WRITE,value);
    break;
    case Memory::TYPE_MEMORY:
        //ndebug("size:"+std::to_string(size)+"\n");
        address-=r->address_start;
        for(int i=0;i<size;i++){
            r->data[address+i] = (value >> i*8) & 0xFF;
        }
    break;
    default:
        ndebug("ERROR VIDEO - Type not implemented: 0x"+to_hex(address)+"type:"+std::to_string(r->type)+"\n");
        return false;
        break;
    }

    return true;
}

bool PPUMemory::write(unsigned int address, unsigned char value)
{
    //ndebug("Writing ppu:"+to_hex(address)+" value: "+to_hex(value)+"\n");
    return write(address,1,((uint)value) & 0xff);
}

bool PPUMemory::read(unsigned int address, unsigned char size, unsigned int &value)
{
    Memory::region* r = get_region(address);

    if(r==nullptr){
        ndebug("ERROR VIDEO: Reading unmapped memory: 0x"+to_hex(address)+"\n");
        return false;
    }

    if((r->mode & Memory::MODE_READ) == 0){
        ndebug("ERROR VIDEO: Reading write-only memory: 0x"+to_hex(address)+"\n");
        return false;
    }

    switch (r->type) {
    case Memory::TYPE_CALLBACK:
        r->callback(address,size,Memory::MODE_READ,value);
    break;
    case Memory::TYPE_MEMORY:
        value=0;
        address-=r->address_start;
        for(int i=0;i<size;i++){
            value |= r->data.at(address+i) << i*8;
        }
    break;
    default:
        ndebug("ERROR VIDEO - Type not implemented: 0x"+to_hex(address)+" type:"+std::to_string(r->type)+"\n");
        return false;
        break;
    }

    return true;
}

bool PPUMemory::read(unsigned int address, unsigned short &value)
{
    uint v;
    bool r = read(address,2,v);
    if(!r)
        return false;
    value = (uint16)v;
    return true;
}

bool PPUMemory::read(unsigned int address, unsigned char &value)
{
    uint v;
    bool r = read(address,1,v);
    if(!r)
        return false;
    value = (BYTE)v;
    return true;
}

void PPUMemory::map_region(unsigned int start_address, unsigned int size, unsigned char mode, std::vector<unsigned char> data)
{
    Memory::region *r;
    r=new Memory::region;
    r->address_start = start_address;
    r->address_end = start_address+size-1;
    r->mode = mode;
    r->data = data;
    r->type = Memory::TYPE_MEMORY;
    space.push_back(r);
}

Memory::region *PPUMemory::get_region(unsigned int address)
{
    Memory::region *r = nullptr;
    for(size_t i=0;i<space.size();i++){
        if(address>=space.at(i)->address_start && address<=space.at(i)->address_end){
            r = space.at(i);
            break;
        }
    }
    return r;
}
