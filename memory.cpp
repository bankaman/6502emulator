#include "memory.h"

std::vector<Memory::region*> Memory::space;

void Memory::map_region(unsigned int start_address, unsigned int size, unsigned char mode, std::vector<unsigned char> data)
{
    region *r;
    r=new region;
    r->address_start = start_address;
    r->address_end = start_address+size-1;
    r->mode = mode;
    r->data = data;
    r->type = TYPE_MEMORY;
    space.push_back(r);
}

bool Memory::read(unsigned int address, BYTE size, unsigned int &value)
{

    region* r = get_region(address);

    if(r==nullptr){
        ndebug("ERROR: Reading unmapped memory: 0x"+to_hex(address)+"\n");
        return false;
    }

    if((r->mode & MODE_READ) == 0){
        ndebug("ERROR: Reading write-only memory: 0x"+to_hex(address)+"\n");
        return false;
    }

    switch (r->type) {
    case TYPE_CALLBACK:
        r->callback(address,size,MODE_READ,value);
    break;
    case TYPE_MEMORY:
        value=0;
        address-=r->address_start;
        for(int i=0;i<size;i++){
            value |= r->data.at(address+i) << i*8;
        }
    break;
    default:
        ndebug("Type not implemented: 0x"+to_hex(address)+" type:"+std::to_string(r->type)+"\n");
        return false;
        break;
    }

    return true;
}

bool Memory::read(unsigned int address, unsigned short &value)
{
    uint v;
    bool r = read(address,2,v);
    if(!r)
        return false;
    value = (uint16)v;
    return true;
}

bool Memory::read(unsigned int address, unsigned int &value)
{
    return read(address,2,value);
}

bool Memory::read(unsigned int address, BYTE &value)
{
    uint v;
    bool r = read(address,1,v);
    if(!r)
        return false;
    value = (BYTE)v;
    return true;
}

bool Memory::write(uint address, BYTE size, uint value)
{
    region* r = get_region(address);

    if(r==nullptr){
        ndebug("ERROR: Writing unmapped memory: 0x"+to_hex(address)+"\n");
        return false;
    }

    if((r->mode & MODE_WRITE) == 0){
        ndebug("ERROR: Writing read-only memory: 0x"+to_hex(address)+"\n");
        return false;
    }

    switch (r->type) {
    case TYPE_CALLBACK:
        r->callback(address,size,MODE_WRITE,value);
    break;
    case TYPE_MEMORY:
        //ndebug("size:"+std::to_string(size)+"\n");
        address-=r->address_start;
        for(int i=0;i<size;i++){
            r->data[address+i] = (value >> i*8) & 0xFF;
        }
    break;
    default:
        ndebug("Type not implemented: 0x"+to_hex(address)+"type:"+std::to_string(r->type)+"\n");
        return false;
        break;
    }

    return true;
}

bool Memory::write(uint address, BYTE value)
{
    return write(address,1,((uint)value) & 0xff);
}

bool Memory::write(uint address, uint16 value)
{
    return write(address,2,((uint)value) & 0xffff);
}

void Memory::map_callback(unsigned int start_address, unsigned int size, unsigned char mode, memory_access_map_callback(callback))
{
    region *r;
    r=new region;
    r->address_start = start_address;
    r->address_end = start_address+size-1;
    r->mode = mode;
    r->callback = callback;
    r->type = TYPE_CALLBACK;
    space.push_back(r);
}

void Memory::clear()
{
    for(size_t i=0;i<space.size();i++){
        delete space.at(i);
    }
    space.clear();
}

Memory::region *Memory::get_region(unsigned int address)
{
    region *r = nullptr;
    for(size_t i=0;i<space.size();i++){
        if(address>=space.at(i)->address_start && address<=space.at(i)->address_end){
            r = space.at(i);
            break;
        }
    }
    return r;
}
