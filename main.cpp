#include <iostream>
#include <thread>
#include <string>

#include <defines.h>
#include <fs.h>
#include <memory.h>
#include <cartridge.h>
#include <cpu.h>
#include <ncinterface.h>
#include <ppu.h>
#include <ppumemory.h>

#include "sdl/palleteview.h"
#include "fs.h"
#include "disasm.h"



using namespace std;

void sdl_main(){
    sdl::PalleteView win;
    win.create_window(0,0,800,600,false,false);
    win.create_renderer();
    win.init();
    while (true){
        win.pre_render();
    }
}

void print_usage(char* progname){
    cout<<"ussage: "<<progname<<" <filename>"<<std::endl;
    cout<<"ussage: "<<progname<<" -disasm <filename>"<<std::endl;
}

int main(int argc, char** argv)
{
    if(argc < 2){
        print_usage(argv[0]);
        return 1;
    }

    if(strcmp(argv[1],"-disasm")==0){
        if(argc < 3){
            print_usage(argv[0]);
            return 1;
        }

        ByteArray t;
        t=FS::readFile(argv[2]);
        Memory::map_region(0x0000,t.size(),Memory::MODE_READ,t);


        cout<<Disasm::do_disasm(0,100)<<endl;
    }

    if(!Cartridge::load(argv[1])){
        std_debug("Not nes file!");
        return 1;
    }
    //Memory::map_region(0x8000,0x4000,(Memory::MODE_EXECUTE|Memory::MODE_READ),first_page);
    Memory::map_callback(0x8000,0x8000,Memory::MODE_EXECUTE|Memory::MODE_READ, &Cartridge::access);
    Memory::map_callback(0x2000,0x8,Memory::MODE_READ|Memory::MODE_WRITE, &PPU::access);

    //RAM
    ByteArray RAM(0x800);
    Memory::map_region(0x0,0x800,Memory::MODE_WRITE|Memory::MODE_READ|Memory::MODE_EXECUTE,RAM);

    //PPU
    ByteArray PPU_RAM(0x1000);
    ByteArray Pallete(0x100);
    PPUMemory::map_region(0x2000,0x1000,Memory::MODE_WRITE|Memory::MODE_READ,PPU_RAM);
    PPUMemory::map_region(0x3F00,0x100,Memory::MODE_WRITE|Memory::MODE_READ,Pallete);

    thread sdl_thread(sdl_main);
    NCInterface::run();

    sdl_thread.join();

    Cartridge::free();
    Memory::clear();
    return 0;
}
