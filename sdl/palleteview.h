#ifndef PALLETEVIEW_H
#define PALLETEVIEW_H

#include "sdlmain.h"
#include "defines.h"

namespace sdl {

class PalleteView : public SDLMain
{
public:
    PalleteView();
    ~PalleteView();
    void init();

    const int MODE_PALLETE     = 0;
    const int MODE_BACKGROUND  = 1;

    const int MODE_MAX  = 2;

private:

    void rendering(SDL_Renderer *r);

    void draw_pallete(SDL_Renderer *r);
    void draw_all_background(SDL_Renderer *r);

    void set_color(SDL_Renderer *r,BYTE index);
    void index_color_to_rgb(BYTE index, BYTE &r, BYTE &g, BYTE &b);

    int mode;

    SDL_Texture *nametables;
    std::vector<BYTE> nametables_arr;

    void vir_input_handler(SDL_Event &e);
};


}

#endif // PALLETEVIEW_H
