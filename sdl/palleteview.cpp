#include "palleteview.h"
#include "ppumemory.h"
#include "ppu.h";

using namespace sdl;
PalleteView::PalleteView()
{
    mode=MODE_BACKGROUND;
    nametables = nullptr;
}

PalleteView::~PalleteView()
{
    SDL_DestroyTexture(nametables);
}

void PalleteView::rendering(SDL_Renderer *r)
{    

    if(mode==MODE_PALLETE)
        draw_pallete(r);

    if(mode==MODE_BACKGROUND)
        draw_all_background(r);

}

void PalleteView::draw_pallete(SDL_Renderer *r)
{
    SDL_Rect rect;
    rect.x=0;
    rect.y=0;
    rect.w=50;
    rect.h=50;
    //SDL_SetRenderDrawColor( r, 0, 0, 255, 255 );

    //bg color
    BYTE color;
    PPUMemory::read(0x3F00,color);
    set_color(r,color);
    SDL_RenderFillRect(r,&rect);

    rect.y+=50;
    //bg pallete 1
    for(uint16 i=0;i<3;i++){
        PPUMemory::read(0x3F01+i,color);
        set_color(r,color);
        SDL_RenderFillRect(r,&rect);
        rect.x+=50;
    }
    rect.x+=20;
    //bg pallete 2
    for(uint16 i=0;i<3;i++){
        PPUMemory::read(0x3F05+i,color);
        set_color(r,color);
        SDL_RenderFillRect(r,&rect);
        rect.x+=50;
    }
    rect.x+=20;
    //bg pallete 3
    for(uint16 i=0;i<3;i++){
        PPUMemory::read(0x3F09+i,color);
        set_color(r,color);
        SDL_RenderFillRect(r,&rect);
        rect.x+=50;
    }
    rect.x+=20;
    //bg pallete 4
    for(uint16 i=0;i<3;i++){
        PPUMemory::read(0x3F0D+i,color);
        set_color(r,color);
        SDL_RenderFillRect(r,&rect);
        rect.x+=50;
    }

    rect.y+=50;
    rect.x=0;
    //sprite pallete 1
    for(uint16 i=0;i<3;i++){
        PPUMemory::read(0x3F11+i,color);
        set_color(r,color);
        SDL_RenderFillRect(r,&rect);
        rect.x+=50;
    }
    rect.x+=20;
    //sprite pallete 2
    for(uint16 i=0;i<3;i++){
        PPUMemory::read(0x3F15+i,color);
        set_color(r,color);
        SDL_RenderFillRect(r,&rect);
        rect.x+=50;
    }
    rect.x+=20;
    //sprite pallete 3
    for(uint16 i=0;i<3;i++){
        PPUMemory::read(0x3F19+i,color);
        set_color(r,color);
        SDL_RenderFillRect(r,&rect);
        rect.x+=50;
    }
    //sprite pallete 4
    rect.x+=20;
    for(uint16 i=0;i<3;i++){
        PPUMemory::read(0x3F1D+i,color);
        set_color(r,color);
        SDL_RenderFillRect(r,&rect);
        rect.x+=50;
    }





    SDL_SetRenderDrawColor( r, 0, 0, 0, 0 );
}

void PalleteView::draw_all_background(SDL_Renderer *r)
{
    if(nametables==nullptr){
        nametables=SDL_CreateTexture(r,SDL_PIXELFORMAT_RGB24,SDL_TEXTUREACCESS_STREAMING,512,480);
        nametables_arr.resize(512*480*3);
    }

    SDL_Rect rect;
    rect.x=0;
    rect.y=0;
    rect.w=512;
    rect.h=480;

    for(int screen=0;screen<4;screen++){

        int address=0x2000;
        int base_x=0;
        int base_y=0;

        if(screen==1){
            address=0x2400;
            base_x=256;
        }
        if(screen==2){
            address=0x2800;
            base_y = 240;
        }
        if(screen==3){
            address=0x2C00;
            base_x=256;
            base_y=240;
        }



        int x=0,y=0;
        BYTE tile_id=0;
        BYTE tile_data_1;
        BYTE tile_data_2;
        int tile_address;
        int pallete_id;
        BYTE attribute_data;
        int pixel_x,pixel_y;
        BYTE color_index;
        BYTE red,green,blue;
        BYTE color_id;

        for(int i=0;i<0x3C0;i++){
            PPUMemory::read(address+i,tile_id);
            int attribute_address = 0x3C0+(x/4)+((y/4)*8);
            PPUMemory::read(address+attribute_address,attribute_data);

            if((x/2)%2==0){
                if((y/2)%2==0){
                    //top left
                    pallete_id=0b11&attribute_data;
                }else{
                    //bottom left
                    pallete_id=0b11&(attribute_data>>4);
                }
            }else{
                if((y/2)%2==0){
                    //top right
                    pallete_id=0b11&(attribute_data>>2);
                }else{
                    //bottom right
                    pallete_id=0b11&(attribute_data>>6);
                }
            }

            tile_address=tile_id*2*8+PPU::get_tiles_address();

            for(int line=0;line<8;line++){
                PPUMemory::read(tile_address+line,tile_data_1);
                PPUMemory::read(tile_address+line+8,tile_data_2);
                pixel_y=y*8+line;
                for(int k=7;k>=0;k--){
                    pixel_x=x*8+(7-k);
                    color_index=0;
                    color_index =  (tile_data_1>>k)&1;
                    color_index |= (tile_data_2>>k)&2;

                    if(color_index==0){
                        PPUMemory::read(0x3F00,color_id);
                    }else{
                        if(pallete_id==0){
                            PPUMemory::read(0x3F01+(color_index-1),color_id);
                        }
                        if(pallete_id==1){
                            PPUMemory::read(0x3F05+(color_index-1),color_id);
                        }
                        if(pallete_id==2){
                            PPUMemory::read(0x3F09+(color_index-1),color_id);
                        }
                        if(pallete_id==3){
                            PPUMemory::read(0x3F0D+(color_index-1),color_id);
                        }
                    }
                    red=0;
                    green=0;
                    blue=0;
                    index_color_to_rgb(color_id,red,green,blue);

                    int pixel_addr = ((base_y+pixel_y)*512*3)+((base_x+pixel_x)*3)+0;
                    nametables_arr[pixel_addr]=red;
                    nametables_arr[pixel_addr+1]=green;
                    nametables_arr[pixel_addr+2]=blue;
                }

            }
            //break;

            x++;
            if(x>31){
                x=0;
                y++;
            }
        }

    }

    SDL_UpdateTexture(nametables,&rect,nametables_arr.data(),rect.w*3);
    SDL_RenderCopy(r,nametables,nullptr,nullptr);
}

void PalleteView::set_color(SDL_Renderer *r, unsigned char index)
{
    switch (index) {
    case 0x00: SDL_SetRenderDrawColor( r,0x65,0x65,0x65, 255); break;
    case 0x01: SDL_SetRenderDrawColor( r,0x00,0x2d,0x69, 255); break;
    case 0x02: SDL_SetRenderDrawColor( r,0x13,0x1f,0x7f, 255); break;
    case 0x03: SDL_SetRenderDrawColor( r,0x3c,0x13,0x7c, 255); break;
    case 0x04: SDL_SetRenderDrawColor( r,0x60,0x0b,0x62, 255); break;
    case 0x05: SDL_SetRenderDrawColor( r,0x73,0x0a,0x37, 255); break;
    case 0x06: SDL_SetRenderDrawColor( r,0x71,0x0f,0x07, 255); break;
    case 0x07: SDL_SetRenderDrawColor( r,0x5a,0x1a,0x00, 255); break;
    case 0x08: SDL_SetRenderDrawColor( r,0x34,0x28,0x00, 255); break;
    case 0x09: SDL_SetRenderDrawColor( r,0x0b,0x34,0x00, 255); break;
    case 0x0a: SDL_SetRenderDrawColor( r,0x00,0x3c,0x00, 255); break;
    case 0x0b: SDL_SetRenderDrawColor( r,0x00,0x3d,0x10, 255); break;
    case 0x0c: SDL_SetRenderDrawColor( r,0x00,0x38,0x40, 255); break;
    case 0x0d: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;
    case 0x0e: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;
    case 0x0f: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;

    case 0x10: SDL_SetRenderDrawColor( r,0xae,0xae,0xae, 255); break;
    case 0x11: SDL_SetRenderDrawColor( r,0x0f,0x63,0xb3, 255); break;
    case 0x12: SDL_SetRenderDrawColor( r,0x40,0x51,0xd0, 255); break;
    case 0x13: SDL_SetRenderDrawColor( r,0x78,0x41,0xcc, 255); break;
    case 0x14: SDL_SetRenderDrawColor( r,0xa7,0x36,0xa9, 255); break;
    case 0x15: SDL_SetRenderDrawColor( r,0xc0,0x34,0x70, 255); break;
    case 0x16: SDL_SetRenderDrawColor( r,0xbd,0x3c,0x30, 255); break;
    case 0x17: SDL_SetRenderDrawColor( r,0x9f,0x4a,0x00, 255); break;
    case 0x18: SDL_SetRenderDrawColor( r,0x6d,0x5c,0x00, 255); break;
    case 0x19: SDL_SetRenderDrawColor( r,0x36,0x6d,0x00, 255); break;
    case 0x1a: SDL_SetRenderDrawColor( r,0x07,0x77,0x04, 255); break;
    case 0x1b: SDL_SetRenderDrawColor( r,0x00,0x79,0x3d, 255); break;
    case 0x1c: SDL_SetRenderDrawColor( r,0x00,0x72,0x7d, 255); break;
    case 0x1d: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;
    case 0x1e: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;
    case 0x1f: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;

    case 0x20: SDL_SetRenderDrawColor( r,0xfe,0xfe,0xff, 255); break;
    case 0x21: SDL_SetRenderDrawColor( r,0x5d,0xb3,0xff, 255); break;
    case 0x22: SDL_SetRenderDrawColor( r,0x8f,0xa1,0xff, 255); break;
    case 0x23: SDL_SetRenderDrawColor( r,0xc8,0x90,0xff, 255); break;
    case 0x24: SDL_SetRenderDrawColor( r,0xf7,0x85,0xfa, 255); break;
    case 0x25: SDL_SetRenderDrawColor( r,0xff,0x83,0xc0, 255); break;
    case 0x26: SDL_SetRenderDrawColor( r,0xff,0x8b,0x7f, 255); break;
    case 0x27: SDL_SetRenderDrawColor( r,0xef,0x9a,0x49, 255); break;
    case 0x28: SDL_SetRenderDrawColor( r,0xbd,0xac,0x2c, 255); break;
    case 0x29: SDL_SetRenderDrawColor( r,0x85,0xbc,0x2f, 255); break;
    case 0x2a: SDL_SetRenderDrawColor( r,0x55,0xc7,0x53, 255); break;
    case 0x2b: SDL_SetRenderDrawColor( r,0x3c,0xc9,0x8c, 255); break;
    case 0x2c: SDL_SetRenderDrawColor( r,0x3e,0xc2,0xcd, 255); break;
    case 0x2d: SDL_SetRenderDrawColor( r,0x4e,0x4e,0x4e, 255); break;
    case 0x2e: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;
    case 0x2f: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;

    case 0x30: SDL_SetRenderDrawColor( r,0xfe,0xfe,0xff, 255); break;
    case 0x31: SDL_SetRenderDrawColor( r,0xbc,0xdf,0xff, 255); break;
    case 0x32: SDL_SetRenderDrawColor( r,0xd1,0xd8,0xff, 255); break;
    case 0x33: SDL_SetRenderDrawColor( r,0xe8,0xd1,0xff, 255); break;
    case 0x34: SDL_SetRenderDrawColor( r,0xfb,0xcd,0xfd, 255); break;
    case 0x35: SDL_SetRenderDrawColor( r,0xff,0xcc,0xe5, 255); break;
    case 0x36: SDL_SetRenderDrawColor( r,0xff,0xcf,0xca, 255); break;
    case 0x37: SDL_SetRenderDrawColor( r,0xf8,0xd5,0xb4, 255); break;
    case 0x38: SDL_SetRenderDrawColor( r,0xe4,0xdc,0xa8, 255); break;
    case 0x39: SDL_SetRenderDrawColor( r,0xcc,0xe3,0xa9, 255); break;
    case 0x3a: SDL_SetRenderDrawColor( r,0xb9,0xe8,0xb8, 255); break;
    case 0x3b: SDL_SetRenderDrawColor( r,0xae,0xe8,0xd0, 255); break;
    case 0x3c: SDL_SetRenderDrawColor( r,0xaf,0xe5,0xea, 255); break;
    case 0x3d: SDL_SetRenderDrawColor( r,0xb6,0xb6,0xb6, 255); break;
    case 0x3e: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;
    case 0x3f: SDL_SetRenderDrawColor( r,0x00,0x00,0x00, 255); break;
    default:
        SDL_SetRenderDrawColor( r, 0, 0, 255, 255 );
        break;
    }
}

void PalleteView::index_color_to_rgb(unsigned char index, unsigned char &r, unsigned char &g, unsigned char &b)
{
    /*r=0x00;
    g=0xFF;
    b=0x00;
    return;*/
    switch (index) {
        case 0x00: r=0x65; g=0x65; b=0x65; break;
        case 0x01: r=0x00; g=0x2d; b=0x69; break;
        case 0x02: r=0x13; g=0x1f; b=0x7f; break;
        case 0x03: r=0x3c; g=0x13; b=0x7c; break;
        case 0x04: r=0x60; g=0x0b; b=0x62; break;
        case 0x05: r=0x73; g=0x0a; b=0x37; break;
        case 0x06: r=0x71; g=0x0f; b=0x07; break;
        case 0x07: r=0x5a; g=0x1a; b=0x00; break;
        case 0x08: r=0x34; g=0x28; b=0x00; break;
        case 0x09: r=0x0b; g=0x34; b=0x00; break;
        case 0x0a: r=0x00; g=0x3c; b=0x00; break;
        case 0x0b: r=0x00; g=0x3d; b=0x10; break;
        case 0x0c: r=0x00; g=0x38; b=0x40; break;
        case 0x0d: r=0x00; g=0x00; b=0x00; break;
        case 0x0e: r=0x00; g=0x00; b=0x00; break;
        case 0x0f: r=0x00; g=0x00; b=0x00; break;

        case 0x10: r=0xae; g=0xae; b=0xae; break;
        case 0x11: r=0x0f; g=0x63; b=0xb3; break;
        case 0x12: r=0x40; g=0x51; b=0xd0; break;
        case 0x13: r=0x78; g=0x41; b=0xcc; break;
        case 0x14: r=0xa7; g=0x36; b=0xa9; break;
        case 0x15: r=0xc0; g=0x34; b=0x70; break;
        case 0x16: r=0xbd; g=0x3c; b=0x30; break;
        case 0x17: r=0x9f; g=0x4a; b=0x00; break;
        case 0x18: r=0x6d; g=0x5c; b=0x00; break;
        case 0x19: r=0x36; g=0x6d; b=0x00; break;
        case 0x1a: r=0x07; g=0x77; b=0x04; break;
        case 0x1b: r=0x00; g=0x79; b=0x3d; break;
        case 0x1c: r=0x00; g=0x72; b=0x7d; break;
        case 0x1d: r=0x00; g=0x00; b=0x00; break;
        case 0x1e: r=0x00; g=0x00; b=0x00; break;
        case 0x1f: r=0x00; g=0x00; b=0x00; break;

        case 0x20: r=0xfe; g=0xfe; b=0xff; break;
        case 0x21: r=0x5d; g=0xb3; b=0xff; break;
        case 0x22: r=0x8f; g=0xa1; b=0xff; break;
        case 0x23: r=0xc8; g=0x90; b=0xff; break;
        case 0x24: r=0xf7; g=0x85; b=0xfa; break;
        case 0x25: r=0xff; g=0x83; b=0xc0; break;
        case 0x26: r=0xff; g=0x8b; b=0x7f; break;
        case 0x27: r=0xef; g=0x9a; b=0x49; break;
        case 0x28: r=0xbd; g=0xac; b=0x2c; break;
        case 0x29: r=0x85; g=0xbc; b=0x2f; break;
        case 0x2a: r=0x55; g=0xc7; b=0x53; break;
        case 0x2b: r=0x3c; g=0xc9; b=0x8c; break;
        case 0x2c: r=0x3e; g=0xc2; b=0xcd; break;
        case 0x2d: r=0x4e; g=0x4e; b=0x4e; break;
        case 0x2e: r=0x00; g=0x00; b=0x00; break;
        case 0x2f: r=0x00; g=0x00; b=0x00; break;

        case 0x30: r=0xfe; g=0xfe; b=0xff; break;
        case 0x31: r=0xbc; g=0xdf; b=0xff; break;
        case 0x32: r=0xd1; g=0xd8; b=0xff; break;
        case 0x33: r=0xe8; g=0xd1; b=0xff; break;
        case 0x34: r=0xfb; g=0xcd; b=0xfd; break;
        case 0x35: r=0xff; g=0xcc; b=0xe5; break;
        case 0x36: r=0xff; g=0xcf; b=0xca; break;
        case 0x37: r=0xf8; g=0xd5; b=0xb4; break;
        case 0x38: r=0xe4; g=0xdc; b=0xa8; break;
        case 0x39: r=0xcc; g=0xe3; b=0xa9; break;
        case 0x3a: r=0xb9; g=0xe8; b=0xb8; break;
        case 0x3b: r=0xae; g=0xe8; b=0xd0; break;
        case 0x3c: r=0xaf; g=0xe5; b=0xea; break;
        case 0x3d: r=0xb6; g=0xb6; b=0xb6; break;
        case 0x3e: r=0x00; g=0x00; b=0x00; break;
        case 0x3f: r=0x00; g=0x00; b=0x00; break;
        default:
            r=0xFF; g=0xFF; b=0xFF;
            break;
    }
}

void PalleteView::vir_input_handler(SDL_Event &e)
{
    if(e.type == SDL_KEYDOWN){
        if(e.key.keysym.sym==SDLK_RIGHT){
            mode++;
            if(mode>=MODE_MAX) mode=0;
        }
        if(e.key.keysym.sym==SDLK_LEFT){
            mode--;
            if(mode<0) mode=MODE_MAX-1;
        }
    }
}

void PalleteView::init()
{
    //t = SDL_CreateTexture(render,SDL_PIXELFORMAT_RGB24,SDL_TEXTUREACCESS_STREAMING,100,100);

}
