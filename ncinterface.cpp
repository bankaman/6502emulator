#include "ncinterface.h"
#include "disasm.h"
#include "cpu.h"
#include "ppu.h"
#include "memory.h"

NCInterface::TPanel NCInterface::debug;
NCInterface::TPanel NCInterface::disasm;
NCInterface::TPanel NCInterface::cpu_state;
NCInterface::TPanel NCInterface::ppu_state;
NCInterface::TPanel NCInterface::memory_view;

int NCInterface::disasm_address=0;
int NCInterface::memory_address_cpu=0;
int NCInterface::memory_address_ppu=0;

bool NCInterface::memview_cpu=true;

void NCInterface::run()
{
    initscr();                   // Переход в curses-режим
    noecho();
    raw();                          /* Line buffering disabled      */
    keypad(stdscr, TRUE);

    start_color();
    init_pair(2,COLOR_BLUE,COLOR_BLACK);

    debug.init("debug, F5 - step, F6 - run, F7 - reset");
    disasm.init("disasm");
    cpu_state.init("cpu");
    ppu_state.init("ppu");
    memory_view.init("memory(CPU)");
    memory_view.selected=true;

    resize();

    int key=0;

    //init

    CPU::reset();
    //ndebug("hellol\n");
    disasm_address=CPU::get_pc();


    while(true){
        if(KEY_RESIZE==key){
            resize();
        }

        if(key=='\t'){
            if(disasm.selected){
                disasm.selected=false;
                memory_view.selected=true;
            }else
            if(memory_view.selected){
                memory_view.selected=false;
                disasm.selected=true;
            }

        }

        //ndebug("key:"+std::to_string(key)+"|"+std::to_string(KEY_F(5))+"\n");
        if(key==KEY_F(5)){
            CPU::step();
            disasm_address=CPU::get_pc();
            if(memview_cpu)
                memory_address_cpu=CPU::get_s()+0x100;
        }

        if(key==KEY_F(6)){
            while(CPU::step()){}
            disasm_address=CPU::get_pc();
        }
        if(key==KEY_F(7)){
            CPU::reset();
            disasm_address=CPU::get_pc();
        }

        if(key=='s'){
            if(memory_view.selected)
                memory_address_cpu=CPU::get_s()+0x100;
        }

        if(key=='p'){
            if(disasm.selected)
                disasm_address=CPU::get_pc();
            if(memory_view.selected){
                memview_cpu=!memview_cpu;
                memory_view.header = memview_cpu ? "memory(CPU)" : "memory(PPU)";
            }

        }

        if(key==KEY_UP){
            if(memory_view.selected)
                if(memview_cpu)
                    memory_address_cpu-=0x10;
                else
                    memory_address_ppu-=0x10;
            if(disasm.selected)
                disasm_address-=1;
        }
        if(key==KEY_DOWN){
            if(memory_view.selected)
                if(memview_cpu)
                    memory_address_cpu+=0x10;
                else
                    memory_address_ppu+=0x10;
            if(disasm.selected)
                disasm_address+=1;
        }

        if(key==KEY_LEFT){
            if(memory_view.selected)
                if(memview_cpu)
                    memory_address_cpu--;
                else
                    memory_address_ppu--;
        }
        if(key==KEY_RIGHT){
            if(memory_view.selected)
                if(memview_cpu)
                    memory_address_cpu++;
                else
                    memory_address_ppu++;
        }

        if(key==KEY_NPAGE){
            if(memory_view.selected)
                if(memview_cpu)
                    memory_address_cpu+=0x10*(getmaxy(memory_view.window)-2);
                else
                    memory_address_ppu+=0x10*(getmaxy(memory_view.window)-2);
        }
        if(key==KEY_PPAGE){
            if(memory_view.selected)
                if(memview_cpu)
                    memory_address_cpu-=0x10*(getmaxy(memory_view.window)-2);
                else
                    memory_address_ppu-=0x10*(getmaxy(memory_view.window)-2);
        }

        disasm.buffer=Disasm::do_disasm(disasm_address,getmaxy(disasm.window)-1);


        debug.update();
        disasm.update();
        update_cpu_state();
        update_ppu_state();
        update_memory_view();
        doupdate();
        refresh();

        key=getch();
    }
    endwin();                    // Выход из curses-режима. Обязательная команда.
    return;
}

void NCInterface::resize()
{
    int row,col;
    getmaxyx(stdscr, row, col);
    debug.resize(row/3,col);
    debug.move(row-row/3,0);

    disasm.resize(row-row/3,col/4);
    disasm.move(0,0);

    cpu_state.resize(11,20);
    cpu_state.move(0,col/4);

    ppu_state.resize(13,20);
    ppu_state.move(0,col/4+20);

    memory_view.move(0,col/4+40);
    memory_view.resize(row-row/3,55);
}

void NCInterface::update_cpu_state()
{

    cpu_state.buffer=" PC : "+to_hex(CPU::get_pc())+"\n";
    cpu_state.append("  A : "+to_hex(CPU::get_a())+"\n");
    cpu_state.append("  X : "+to_hex(CPU::get_x())+"\n");
    cpu_state.append("  Y : "+to_hex(CPU::get_y())+"\n");
    cpu_state.append("  S : "+to_hex(CPU::get_s())+"\n");
    BYTE P = CPU::get_p();
    cpu_state.append("  P : "+to_hex(P)+"\n");
    cpu_state.append("\n");

    cpu_state.append(" N V 1 B D I Z C \n");
    cpu_state.append(by_bit(P));

    cpu_state.update();


}

void NCInterface::update_ppu_state()
{
    BYTE PPUCTRL=PPU::get_PPUCTRL();
    ppu_state.buffer="(2000)CTRL : "+to_hex(PPUCTRL)+"\n";
    ppu_state.append(" V P H B S I N N \n");
    ppu_state.append(by_bit(PPUCTRL)+"\n");

    BYTE PPUSTATUS=PPU::get_PPUSTATUS();
    ppu_state.buffer+="(2002)STATUS : "+to_hex(PPUSTATUS)+"\n";
    ppu_state.append(" V S O - - - - - \n");
    ppu_state.append(by_bit(PPUSTATUS)+"\n");

    BYTE PPUMASK=PPU::get_PPUMASK();
    ppu_state.buffer+="(2001)MASK : "+to_hex(PPUMASK)+"\n";
    ppu_state.append(" B G R s b M m G \n");
    ppu_state.append(by_bit(PPUMASK)+"\n");

    ppu_state.buffer+="(2003)OAMADDR:"+to_hex(PPU::get_OAM_ADDR())+"\n";

    ppu_state.buffer+="(2005)SCROLX:"+to_hex(PPU::get_SCROLL_X())+"\n";
    ppu_state.buffer+="(2005)SCROLY:"+to_hex(PPU::get_SCROLL_Y())+"\n";

    ppu_state.buffer+="(2006)ADDR:"+to_hex(PPU::get_PPUADDR())+"\n";

    ppu_state.update();
}

void NCInterface::update_memory_view()
{

    memory_view.buffer="      00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F";

    uint start=memview_cpu ? memory_address_cpu : memory_address_ppu;
    std::string adr;
    BYTE t;
    bool suc;
    for(int i=0;i<getmaxy(memory_view.window)-2;i++){
        adr = to_hex(start+i*16);
        while(adr.length()<4)
            adr="0"+adr;
        memory_view.append(adr+":");

        for(int j=0; j < 16;j++){
            if(memview_cpu)
                suc = Memory::read(start+i*16+j,t);
            else
                suc = PPU::memory_read(start+i*16+j,t);
            if(!suc)
                memory_view.append(" ##");
            else{
                adr=to_hex(t);
                while(adr.length()<2)
                    adr="0"+adr;
                memory_view.append(" "+adr);
            }
        }
    }

    memory_view.update();
}

std::string NCInterface::by_bit(BYTE b)
{
    std::string out;
    BYTE t=0b10000000;
    while(t>0){
        out.append(((b&t)>0)?" 1":" 0");
        t>>=1;
    }
    return out;
}

NCInterface::TPanel::~TPanel()
{
    delwin(window);
}

void NCInterface::TPanel::init(std::string data)
{
    w = newwin(10,10,10,10);
    refresh();

    this->header = data;

    window = derwin(w,8,8,1,1);
    scrollok(window,true);
    wmove(w,0,0);
}

void NCInterface::TPanel::resize(int row, int col)
{
    wresize(w,row,col);
    wresize(window,row-2,col-2);
    refresh();
    update();
}

void NCInterface::TPanel::move(int row, int col)
{
    //move_panel(panel,row,col);
    mvwin(w,row,col);
    mvwin(window,row+1,col+1);
    update();
}

void NCInterface::TPanel::update()
{
    wclear(w);
    wclear(window);

    if(selected)
        wattron(w,COLOR_PAIR(2));
    box(w,0,0);
    if(selected)
        wattroff(w,COLOR_PAIR(2));
    mvwprintw(w,0,2,header.c_str());
    wmove(window,0,0);
    mvwprintw(window,0,0,buffer.c_str());

    wrefresh(w);
    wrefresh(window);
}

void NCInterface::TPanel::append(std::string data)
{
    buffer+=data;
    if(buffer.length()>10000){
        buffer=buffer.substr(buffer.length()-10000);
    }
}
