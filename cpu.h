#ifndef CPU_H
#define CPU_H

#include "defines.h"

class CPU
{
public:
    static void reset();
    static bool step();

    static uint16 get_pc(){return PC;}
    static BYTE get_a(){return A;}
    static BYTE get_x(){return X;}
    static BYTE get_y(){return Y;}
    static BYTE get_s(){return S;}
    static BYTE get_p(){return P;}

private:
    static uint16 PC;
    static BYTE A;
    static BYTE X;
    static BYTE Y;
    static BYTE S;
    static BYTE P;

    static void update_ZN_Flags(BYTE value);
    static void clear_C();
    static void clear_D();
    static void set_C();
    static void set_D();
    static void set_V();
    static void clear_V();
    static bool is_zero();
    static bool is_negative();
    static bool is_C_set();
};

#endif // CPU_H
