#include "cartridge.h"

#include "fs.h"
#include "memory.h"
#include "ppumemory.h"

std::vector<ByteArray*> Cartridge::PRG;
std::vector<ByteArray*> Cartridge::CHR;

BYTE Cartridge::CHR_count = 0;
BYTE Cartridge::PRG_count = 0;

ByteArray* Cartridge::prg_block_1 = nullptr;
ByteArray* Cartridge::prg_block_2 = nullptr;

bool Cartridge::load(std::string filename)
{
    free();
    ByteArray cart = FS::readFile(filename);



    if(!(cart.at(0) == 'N' && cart.at(1) == 'E' && cart.at(2) == 'S')){
        std_debug(cart.at(0),cart.at(1),cart.at(2));
        return false;
    }

    uint cur=4;
    PRG_count = cart.at(cur); cur++;
    CHR_count = cart.at(cur); cur++;

    std_debug("Cart OK!","PRG count:",(uint)PRG_count,"CHR count",(uint)CHR_count);
    ndebug("Cart OK! PRG count: "+std::to_string((uint)PRG_count)+" CHR count: "+std::to_string((uint)CHR_count)+"\n");

    cur=0x10;
    ByteArray* t;
    for(int i=0;i<PRG_count;i++){
        t = new ByteArray();
        *t = slice(cart,cur,cur+0x4000-1);
        std_debug(t->size());
        cur+=0x4000;
        PRG.push_back(t);
    }
    std_debug("All PRG was readed!");
    for(int i=0;i<CHR_count;i++){
        t = new ByteArray();
        *t=slice(cart,cur,cur+0x2000-1);
        std_debug(t->size());
        cur+=0x2000;
        CHR.push_back(t);
    }
    std_debug("All CHR was readed!");

    if(PRG_count==1){
        prg_block_1 = PRG.at(0);
        prg_block_2 = PRG.at(0);
    }else
    if(PRG_count==2){
        prg_block_1 = PRG.at(0);
        prg_block_2 = PRG.at(1);
    }else{
        std_debug("Full support of this cartridge was not implemented yet!");
        return false;
    }


    if(CHR_count==1){
        PPUMemory::map_region(0,CHR.at(0)->size(),Memory::MODE_READ|Memory::MODE_WRITE,*CHR.at(0));
    }else{
        std_debug("Full support of this cartridge was not implemented yet!");
        return false;
    }

    return true;
}

void Cartridge::free()
{
    for(size_t i=0;i<PRG.size();i++){
        delete PRG.at(i);
    }
    for(size_t i=0;i<CHR.size();i++){
        delete CHR.at(i);
    }

    PRG_count=0;
    CHR_count=0;

    prg_block_1 = nullptr;
    prg_block_2 = nullptr;
}

void Cartridge::access(unsigned int address, unsigned int size, unsigned int mode, unsigned int &value)
{
    if(address>=0x8000 && address<=0xFFFF){
        //std_debug("is write:",mode & Memory::MODE_WRITE);
        if((mode & Memory::MODE_WRITE) > 0)
            return;

        //std_debug("ok");
        uint loc_address = address - 0x8000;
        ByteArray* current_block;

        if(loc_address>=0x4000){
            //std_debug("block2");
            current_block=prg_block_2;
            loc_address-=0x4000;
        }else{
            //std_debug("block1");
            current_block=prg_block_1;
        }

        //std_debug("addres:",std::hex,loc_address);
        value=0;
        for(int i=0;i<size;i++){
            value |= current_block->at(loc_address+i) << i*8;
        }
    }else{
        std_debug("Reading cartridge at 0x",std::hex," not implemented!");
    }
}
