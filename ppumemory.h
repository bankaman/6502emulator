#ifndef PPUMEMORY_H
#define PPUMEMORY_H

#include "memory.h"

class PPUMemory
{
public:
    static bool write(uint address, BYTE size ,uint value);
    static bool write(uint address, BYTE value);

    static bool read(uint address, BYTE size ,uint &value);
    static bool read(uint address, uint16 &value);
    static bool read(uint address, BYTE &value);

    static void map_region(uint start_address, uint size, BYTE mode, ByteArray data);

private:
    static std::vector<Memory::region*> space;
    static Memory::region* get_region(uint address);
};

#endif // PPUMEMORY_H
