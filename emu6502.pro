TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lpanel -lncurses -lpthread -lSDL2

SOURCES += main.cpp \
    fs.cpp \
    memory.cpp \
    cartridge.cpp \
    defines.cpp \
    cpu.cpp \
    ncinterface.cpp \
    disasm.cpp \
    ppu.cpp \
    ppumemory.cpp \
    sdl/sdlmain.cpp \
    sdl/palleteview.cpp \
    apu.cpp

HEADERS += \
    fs.h \
    defines.h \
    memory.h \
    cartridge.h \
    cpu.h \
    ncinterface.h \
    disasm.h \
    ppu.h \
    ppumemory.h \
    sdl/sdlmain.h \
    sdl/palleteview.h \
    apu.h
