#ifndef PPU_H
#define PPU_H

#include "defines.h"

class PPU
{
public:
    static void access(uint address, uint size , uint mode, uint &value);

    static BYTE get_PPUCTRL(){return PPUCTRL;}
    static BYTE get_PPUSTATUS(){return PPUSTATUS;}
    static BYTE get_PPUMASK(){return PPUMASK;}
    static uint16 get_PPUADDR(){return PPUADDR;}
    static BYTE get_SCROLL_X(){return SCROLL_X;}
    static BYTE get_SCROLL_Y(){return SCROLL_Y;}
    static BYTE get_OAM_ADDR(){return OAM_ADDR;}

    static bool memory_read(uint16 address, BYTE &value);

    static uint16 get_tiles_address();

private:
    static BYTE PPUCTRL;
    static BYTE PPUSTATUS;
    static BYTE PPUMASK;
    static uint16 PPUADDR;
    static bool ppuaddr_h;
    static BYTE SCROLL_X;
    static BYTE SCROLL_Y;
    static bool SCROLL_write_flag;
    static BYTE OAM_ADDR;
    static ByteArray memory;
};

#endif // PPU_H
