#ifndef CARTRIDGE_H
#define CARTRIDGE_H

#include "defines.h"

#include <string>
#include <vector>

class Cartridge
{
public:
    static bool load(std::string filename);
    static void free();

    static void access(uint address, uint size , uint mode, uint &value);

private:
    static std::vector<ByteArray*> PRG;
    static std::vector<ByteArray*> CHR;

    static BYTE CHR_count;
    static BYTE PRG_count;

    static ByteArray* prg_block_1;
    static ByteArray* prg_block_2;
};

#endif // CARTRIDGE_H
