#ifndef FS_H
#define FS_H

#include <vector>
#include <string>

#include <defines.h>

class FS
{
public:    
    static ByteArray readFile(std::string filename);
};

#endif // FS_H
