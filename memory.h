#ifndef MEMORY_H
#define MEMORY_H

#include <defines.h>

class Memory
{

public:

    static const BYTE MODE_READ    = 0b001;
    static const BYTE MODE_WRITE   = 0b010;
    static const BYTE MODE_EXECUTE = 0b101;

    static const BYTE TYPE_MEMORY = 0;
    static const BYTE TYPE_CALLBACK = 1;



    struct region
    {
        uint address_start;
        uint address_end;
        BYTE mode;
        BYTE type;
        ByteArray data;
        memory_access_map_callback(callback);
    };

    static void map_region(uint start_address, uint size, BYTE mode, ByteArray data);
    static void map_callback(uint start_address, uint size, BYTE mode, memory_access_map_callback(callback));

    static bool read(uint address, BYTE size ,uint &value);
    static bool read(uint address, uint16 &value);
    static bool read(uint address, uint &value);
    static bool read(uint address, BYTE &value);

    static bool write(uint address, BYTE size ,uint value);
    static bool write(uint address, BYTE value);
    static bool write(uint address, uint16 value);

    static void clear();

private:
    static std::vector<region*> space;
    static region* get_region(uint address);
};

#endif // MEMORY_H
