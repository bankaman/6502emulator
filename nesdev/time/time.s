JOYPAD1 = $4016
JOYPAD2 = $4017

BUTTON_A      = 1 << 7
BUTTON_B      = 1 << 6
BUTTON_SELECT = 1 << 5
BUTTON_START  = 1 << 4
BUTTON_UP     = 1 << 3
BUTTON_DOWN   = 1 << 2
BUTTON_LEFT   = 1 << 1
BUTTON_RIGHT  = 1 << 0

;
; iNES header
;

.segment "HEADER"

INES_MAPPER = 0 ; 0 = NROM
INES_MIRROR = 1 ; 0 = horizontal mirroring, 1 = vertical mirroring
INES_SRAM   = 0 ; 1 = battery backed SRAM at $6000-7FFF

.byte 'N', 'E', 'S', $1A ; ID
.byte $02 ; 16k PRG chunk count
.byte $00 ; 8k CHR chunk count
.byte INES_MIRROR | (INES_SRAM << 1) | ((INES_MAPPER & $f) << 4)
.byte (INES_MAPPER & %11110000)
.byte $0, $0, $0, $0, $0, $0, $0, $0 ; padding

.segment "OAM"
oam: .res 256        ; sprite OAM data to be uploaded by DMA

.segment "ZEROPAGE"
address:   .res 2
counter:   .res 1
counter2:   .res 1
frame_counter: .res 1
word_to_str_addr: .res 2
word_to_str_result: .res 21 
word_to_str_stable: .res 1
div_value: .res 8
div_mod: .res 8
div_by: .res 8
scroll_x: .res 1
scroll_y: .res 1

timestamp: .res 8
timestamp_incremented: .res 1

buttons: .res 1
readjoy_delay: .res 1


;
; vectors placed at top 6 bytes of memory area
;

.segment "VECTORS"
.word nmi
.word reset
.word irq

;
; reset routine

;

.segment "CODE"
.byte '^', 'T', 'o', 'k', 'i', 'k', 'o', $0
reset:
    sei        ; ignore IRQs
    cld        ; disable decimal mode
    ldx #$40
    stx $4017  ; disable APU frame IRQ
    ldx #$ff
    txs        ; Set up stack
    inx        ; now X = 0
    stx $2000  ; disable NMI
    stx $2001  ; disable rendering
    stx $4010  ; disable DMC IRQs

    ; Optional (omitted):
    ; Set up mapper and jmp to further init code here.

    ; The vblank flag is in an unknown state after reset,
    ; so it is cleared here to make sure that @vblankwait1
    ; does not exit immediately.
    bit $2002

    ; First of two waits for vertical blank to make sure that the
    ; PPU has stabilized
@vblankwait1:  
    bit $2002
    bpl @vblankwait1

    ; We now have about 30,000 cycles to burn before the PPU stabilizes.
    ; One thing we can do with this time is put RAM in a known state.
    ; Here we fill it with $00, which matches what (say) a C compiler
    ; expects for BSS.  Conveniently, X is still 0.
    txa
@clrmem:
    sta $000,x
    sta $100,x
    sta $200,x
    sta $300,x
    sta $400,x
    sta $500,x
    sta $600,x
    sta $700,x
    inx
    bne @clrmem

    ; Other things you can do between vblank waits are set up audio
    ; or set up other mapper registers.
    
    sta scroll_x
    sta scroll_y
    sta word_to_str_result
    sta word_to_str_stable

    sta frame_counter

    sta timestamp
    sta timestamp + 1
    sta timestamp + 2
    sta timestamp + 3
    sta timestamp + 4
    sta timestamp + 5
    sta timestamp + 6
    sta timestamp + 7

    lda #10
    sta readjoy_delay   
    
    lda #1
    sta timestamp_incremented
   
@vblankwait2:
    bit $2002
    bpl @vblankwait2


    
    lda #$11
    ldy #$0
    jsr set_pallete_color
    
    jsr long_wait
    jsr long_wait
    jsr long_wait
    jsr long_wait
    jsr long_wait
    jsr long_wait
    
    
    jsr init
    
    sei
    
@cycle:

    lda timestamp_incremented
    beq :+
        cli
        ldx #.LOBYTE(timestamp)
        stx word_to_str_addr
        ldx #.HIBYTE(timestamp)
        stx word_to_str_addr + 1
       
        jsr word_to_str
        lda #0
        sta timestamp_incremented
        sei
    :

    jmp @cycle
    
nmi:
    pha
    txa
    pha
    tya
    pha

    jsr count_frame_counter
    
    jsr print_timestamp


    jsr controller_actions
    

    jsr set_scroll

    pla
    tay
    pla
    tax
    pla
    rti

irq:
jmp reset

controller_actions:
    dec readjoy_delay
    bne @end

    lda #10
    sta readjoy_delay

    jsr readjoy
    
    lda buttons
        and #BUTTON_DOWN
        beq :+
        jsr add_second
    :
    @end:

rts

readjoy:
    lda #$01
    ; While the strobe bit is set, buttons will be continuously reloaded.
    ; This means that reading from JOYPAD1 will only return the state of the
    ; first button: button A.
    sta JOYPAD1
    sta buttons
    lsr a        ; now A is 0
    ; By storing 0 into JOYPAD1, the strobe bit is cleared and the reloading stops.
    ; This allows all 8 buttons (newly reloaded) to be read from JOYPAD1.
    sta JOYPAD1
    @loop:
    lda JOYPAD1
    lsr a          ; bit 0 -> Carry
    rol buttons  ; Carry -> bit 0; bit 7 -> Carry
        bcc @loop
    rts

print_timestamp:
    lda word_to_str_stable
    beq :+
    
        ldx #$20
        ldy #$63
        jsr set_ppu_address
        
        lda #.HIBYTE(word_to_str_result)
        sta address+1
        lda #.LOBYTE(word_to_str_result)
        sta address
        jsr print_string
        
    :
rts

count_frame_counter:
    inc frame_counter
    lda frame_counter    
    ;cmp #50 ; PAL
    cmp #60 ; NTSE
    bne :+
        lda #0
        sta frame_counter
        jsr add_second
    :
rts

add_second:
    inc timestamp
    bne @end
    inc timestamp + 1
    bne @end
    inc timestamp + 2
    bne @end
    inc timestamp + 3
    bne @end
    inc timestamp + 4
    bne @end
    inc timestamp + 5
    bne @end
    inc timestamp + 6
    bne @end
    inc timestamp + 7
        
    @end:
lda #1
sta timestamp_incremented
rts



word_to_str:
    lda #0
    sta word_to_str_result
    sta word_to_str_stable

    ;load value to divide
    ldy #0
    lda (word_to_str_addr),y
    sta div_value
    iny
    lda (word_to_str_addr),y
    sta div_value + 1
    iny
    lda (word_to_str_addr),y
    sta div_value + 2
    iny
    lda (word_to_str_addr),y
    sta div_value + 3
    iny
    lda (word_to_str_addr),y
    sta div_value + 4
    iny
    lda (word_to_str_addr),y
    sta div_value + 5
    iny
    lda (word_to_str_addr),y
    sta div_value + 6
    iny
    lda (word_to_str_addr),y
    sta div_value + 7
    
    lda #10
    sta div_by
    lda #0
    sta div_by + 1
    sta div_by + 2
    sta div_by + 3
    sta div_by + 4
    sta div_by + 5
    sta div_by + 6
    sta div_by + 7

    :
        jsr divide

        lda div_mod
        adc #48
        pha
        ldy #0
        :
            lda word_to_str_result,y
            tax
            pla
            sta word_to_str_result,y
            iny
            txa
            pha
        bne :-

        pla
        sta word_to_str_result,y

        lda div_value
        ora div_value + 1
        ora div_value + 2
        ora div_value + 3
        ora div_value + 4
        ora div_value + 5
        ora div_value + 6
        ora div_value + 7
    bne :--
    lda #1
    sta word_to_str_stable 
rts
 
div_words:
    
    lda #0
    sta div_mod
    sta div_mod + 1
    clc
    
    ldx #16
    @div_loop:
    
        rol div_value
        rol div_value + 1
        rol div_mod
        rol div_mod + 1
        
        sec 
        lda div_mod
        sbc div_by
        tay 
        lda div_mod + 1
        sbc div_by + 1

        bcc @ignore_result
        
        sty div_mod
        sta div_mod + 1
    
    @ignore_result:
        dex
        bne @div_loop
        
        rol div_value
        rol div_value + 1
        clc
    
rts
 
divide:
    lda #0
    sta div_mod
    sta div_mod + 1
    sta div_mod + 2
    sta div_mod + 3
    sta div_mod + 4
    sta div_mod + 5
    sta div_mod + 6
    sta div_mod + 7
    clc
    
    ldx #64
    @div_loop:
    
        rol div_value
        rol div_value + 1
        rol div_value + 2
        rol div_value + 3
        rol div_value + 4
        rol div_value + 5
        rol div_value + 6
        rol div_value + 7
        
        rol div_mod
        rol div_mod + 1
        rol div_mod + 2
        rol div_mod + 3
        rol div_mod + 4
        rol div_mod + 5
        rol div_mod + 6
        rol div_mod + 7
        
        sec 
        lda div_mod
        sbc div_by
        pha
         
        lda div_mod + 1
        sbc div_by + 1
        pha
        
        lda div_mod + 2
        sbc div_by + 2
        pha
        
        lda div_mod + 3
        sbc div_by + 3
        pha
        
        lda div_mod + 4
        sbc div_by + 4
        pha
        
        lda div_mod + 5
        sbc div_by + 5
        pha
        
        lda div_mod + 6
        sbc div_by + 6
        pha
        
        lda div_mod + 7
        sbc div_by + 7
        pha

        bcc @clear_stack
        
        pla 
        sta div_mod + 7
        pla
        sta div_mod + 6
        pla
        sta div_mod + 5
        pla
        sta div_mod + 4
        pla
        sta div_mod + 3
        pla
        sta div_mod + 2
        pla
        sta div_mod + 1
        pla
        sta div_mod
        jmp @continue
    @clear_stack:
        ldy #8
        :
            pla
            dey
        bne :-
    @continue:
    
        dex
        bne @div_loop
        
        rol div_value
        rol div_value + 1
        rol div_value + 2
        rol div_value + 3
        rol div_value + 4
        rol div_value + 5
        rol div_value + 6
        rol div_value + 7
        
        clc
rts

long_wait:
    ldx #$FF
    ldy #$FF
@waitloop:
    dex
    bne @waitloop
    dey
    bne @waitloop
rts

disable_render:
    lda #0
    sta $2000
    sta $2001
    rts

enable_render:
    lda #%10000000
    sta $2000
    lda #%00001010
    
    sta $2001
    rts

set_ppu_address:
    bit $2002
    stx $2006
    sty $2006
    rts
    
clear_chr_ram:
    ldx #0
    ldy #0
    jsr set_ppu_address
    
    lda #$1F
    sta counter
    lda #$FF
    sta counter2
    
    ldx #0
    
    @loop:
    stx $2007
    
    dec counter2
    bne @loop
    
    dec counter
    bne @loop
    
    rts
    
load_letters_to_ppu:
    lda #.LOBYTE(letters)
    sta address
    lda #.HIBYTE(letters)
    sta address+1
    
    lda #128
    sta counter
    lda #0
    sta counter2
    
    ldx #0
    ldy #0
    jsr set_ppu_address
    
    @loop:
            lda (address),y
            sta $2007
            
            inc counter2
            lda counter2
            cmp #16
            bne :+
                lda #0
                sta counter2
                dec counter
                beq @end
            :
            inc address
        bne @loop
            inc address+1
        jmp @loop
        
    @end:
            
    rts

set_background_tile:
    ldx #$20
    ldy #0
    jsr set_ppu_address
    sta $2007
    rts
    
set_pallete_color:
    ldx #$3F
    jsr set_ppu_address
    sta $2007
    rts
    
load_pallete:
    ;BG
    lda #$0d
    ldy #0
    jsr set_pallete_color
    
    ;pallete 0
    lda #$30
    ldy #1
    jsr set_pallete_color
    lda #$11
    ldy #2
    jsr set_pallete_color
    lda #$19
    ldy #3
    jsr set_pallete_color
    
    ;pallete 1
    lda #$30
    ldy #5
    jsr set_pallete_color
    lda #$11
    ldy #6
    jsr set_pallete_color
    lda #$19
    ldy #7
    
    ;pallete 2
    lda #$30
    ldy #$9
    jsr set_pallete_color
    lda #$11
    ldy #$A
    jsr set_pallete_color
    lda #$19
    ldy #$B
    jsr set_pallete_color
    
    ;pallete 3
    lda #$30
    ldy #$D
    jsr set_pallete_color
    lda #$11
    ldy #$E
    jsr set_pallete_color
    lda #$19
    ldy #$F
    jsr set_pallete_color
    
    rts

set_scroll:
    ldx scroll_x
    stx $2005
    ldx scroll_y
    stx $2005
    rts
    
clear_screen:
    ldx #$20
    ldy #$00
    jsr set_ppu_address
    :
        lda #$20
        :
            sta $2007
            iny 
            bne :-
            
        inx
        txa
        cmp #$24
        bne :--
    rts
    
print_all_tiles:
    ldx #$20
    ldy #$20
    lda #0
    jsr set_ppu_address
    :
        :
            sta $2007
            adc #1
            beq @end
            iny 
            bne :-
            
        inx
        txa
        cmp #$23
        bne :--
    @end:
    rts

increment_address:
    inc address
    bne @end
        inc address+1
    @end:
    rts
    
print_string:
    :
        ldy #0
        lda (address),y
        beq @end
            
        sta $2007
        jsr increment_address
    jmp :-
    
    @end:
    rts
    

print_hello_world:
    jsr clear_screen
    
    ldx #$20
    ldy #$22
    jsr set_ppu_address
    
    lda #$80
    sta address+1
    lda #0
    sta address
    jsr print_string
    
    ldx #$20
    ldy #$42
    jsr set_ppu_address
    
    lda #.HIBYTE(hello_world_str)
    sta address+1
    lda #.LOBYTE(hello_world_str)
    sta address
    jsr print_string
rts

init:
    ;jsr clear_chr_ram
    jsr load_pallete
    jsr load_letters_to_ppu
    
    jsr print_hello_world

    ;lda #$20
    ;sta $2007  
    ;lda #$20
    ;sta $2007  
    ;lda #$21
    ;sta $2007  
    ;lda #$21
    ;sta $2007  
   
    jsr enable_render
    
    rts

.include "letters.inc"

hello_world_str:
.byte "Hello World!",$0
current_timestamp_str:
.byte "Currnt timestamp:",$0
