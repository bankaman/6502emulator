.segment "CODE"

letters:
    ;00
    .byte %11111111
    .byte %11000011
    .byte %10100101
    .byte %10011001
    .byte %10011001
    .byte %10100101
    .byte %11000011
    .byte %11111111
    .byte 0,0,0,0,0,0,0,0
    
    .res (31*16), $55

    ; 20
    .byte 0,0,0,0,0,0,0,0
    .byte 0,0,0,0,0,0,0,0

    ; 21 !
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 22 "
    .byte %00000000
    .byte %01101100
    .byte %01101100
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 23 #
    .byte %00000000
    .byte %00100100
    .byte %01111110
    .byte %00100100
    .byte %00100100
    .byte %01111110
    .byte %00100100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 24 $
    .byte %00010000
    .byte %00111100
    .byte %01010000
    .byte %00111000
    .byte %00010100
    .byte %01111000
    .byte %00010000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 25 %
    .byte %01100000
    .byte %01010010
    .byte %00110100
    .byte %00001000
    .byte %00010000
    .byte %00101100
    .byte %01001010
    .byte %00000110
    .byte 0,0,0,0,0,0,0,0
    
    ; 26 &
    .byte %00000000
    .byte %00111000
    .byte %01000100
    .byte %00111000
    .byte %01010000
    .byte %01001010
    .byte %00111100
    .byte %00000010
    .byte 0,0,0,0,0,0,0,0
    
    ; 27 '
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 28 (
    .byte %00000000
    .byte %00011000
    .byte %01100000
    .byte %01100000
    .byte %01100000
    .byte %01100000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 29 )
    .byte %00000000
    .byte %00011000
    .byte %00000110
    .byte %00000110
    .byte %00000110
    .byte %00000110
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 2A *
    .byte %00000000
    .byte %00010000
    .byte %01010100
    .byte %00111000
    .byte %00111000
    .byte %01010100
    .byte %00010000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 2B +
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %01111110
    .byte %01111110
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 2C ,
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %00110000
    .byte 0,0,0,0,0,0,0,0
    
    ; 2D -
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %01111110
    .byte %01111110
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 2E .
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 2F /
    .byte %00000000
    .byte %00000000
    .byte %00000110
    .byte %00001100
    .byte %00011000
    .byte %00110000
    .byte %01100000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 30 0
    .byte %00000000
    .byte %00111100
    .byte %01100110
    .byte %01110110
    .byte %01101110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 31 1
    .byte %00000000
    .byte %00011000
    .byte %00111000
    .byte %01111000
    .byte %00011000
    .byte %00011000
    .byte %01111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 32 2
    .byte %00000000
    .byte %00111000
    .byte %01101100
    .byte %00001100
    .byte %00011000
    .byte %00110000
    .byte %01111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 33 3
    .byte %00000000
    .byte %00111000
    .byte %01101100
    .byte %00011000
    .byte %00001100
    .byte %01101100
    .byte %00111000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 34 4
    .byte %00000000
    .byte %00011100
    .byte %00111100
    .byte %01101100
    .byte %01111110
    .byte %00001100
    .byte %00001100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 35 5
    .byte %00000000
    .byte %01111110
    .byte %01100000
    .byte %01111100
    .byte %00000110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 36 6
    .byte %00000000
    .byte %00111100
    .byte %01100000
    .byte %01111100
    .byte %01100110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 37 7
    .byte %00000000
    .byte %01111110
    .byte %00000110
    .byte %00001100
    .byte %00001100
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 38 8
    .byte %00000000
    .byte %00111100
    .byte %01100110
    .byte %00111100
    .byte %01100110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 39 9
    .byte %00000000
    .byte %00111100
    .byte %01100110
    .byte %00111110
    .byte %00000110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 3A :
    .byte %00000000
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 3B ;
    .byte %00000000
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %00110000
    .byte 0,0,0,0,0,0,0,0
    
    ; 3C <
    .byte %00000000
    .byte %00011000
    .byte %00110000
    .byte %01100000
    .byte %01100000
    .byte %00110000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 3D = 
    .byte %00000000
    .byte %00000000
    .byte %01111110
    .byte %01111110
    .byte %00000000
    .byte %01111110
    .byte %01111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 3E >
    .byte %00000000
    .byte %00011000
    .byte %00001100
    .byte %00000110
    .byte %00000110
    .byte %00001100
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 3F ?
    .byte %00000000
    .byte %00111000
    .byte %01101100
    .byte %00001100
    .byte %00011000
    .byte %00000000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 40 @
    .byte %00000000
    .byte %00111100
    .byte %01000010
    .byte %01011010
    .byte %01011100
    .byte %01000000
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 41 A
    .byte %00000000
    .byte %00011000
    .byte %00111100
    .byte %01100110
    .byte %01111110
    .byte %01100110
    .byte %01100110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 42 B
    .byte %00000000
    .byte %01111100
    .byte %01100110
    .byte %01111100
    .byte %01100110
    .byte %01100110
    .byte %01111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0

    ; 43 C
    .byte %00000000
    .byte %00111110
    .byte %01100000
    .byte %01100000
    .byte %01100000
    .byte %01100000
    .byte %00111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 44 D
    .byte %00000000
    .byte %01111100
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %01111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 45 E
    .byte %00000000
    .byte %01111110
    .byte %01100000
    .byte %01111100
    .byte %01100000
    .byte %01100000
    .byte %01111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 46 F
    .byte %00000000
    .byte %01111110
    .byte %01100000
    .byte %01100000
    .byte %01111100
    .byte %01100000
    .byte %01100000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 47 G
    .byte %00000000
    .byte %00111100
    .byte %01100110
    .byte %01100000
    .byte %01101110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 48 H
    .byte %00000000
    .byte %01100110
    .byte %01100110
    .byte %01111110
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 49 I
    .byte %00000000
    .byte %01111110
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %01111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 4A J
    .byte %00000000
    .byte %00111110
    .byte %00001100
    .byte %00001100
    .byte %00001100
    .byte %01101100
    .byte %00111000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 4B K
    .byte %00000000
    .byte %01100110
    .byte %01101100
    .byte %01111000
    .byte %01111000
    .byte %01101100
    .byte %01100110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 4C L
    .byte %00000000
    .byte %01100000
    .byte %01100000
    .byte %01100000
    .byte %01100000
    .byte %01100000
    .byte %01111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 4D M
    .byte %00000000
    .byte %01000010
    .byte %01100110
    .byte %01111110
    .byte %01011010
    .byte %01000010
    .byte %01000010
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 4E N
    .byte %00000000
    .byte %01100110
    .byte %01110110
    .byte %01110110
    .byte %01101110
    .byte %01101110
    .byte %01100110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 4F O
    .byte %00000000
    .byte %00111100
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 50 P
    .byte %00000000
    .byte %01111100
    .byte %01100110
    .byte %01100110
    .byte %01111100
    .byte %01100000
    .byte %01100000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 51 Q
    .byte %00000000
    .byte %00111100
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %01101110
    .byte %00111100
    .byte %00000110
    .byte 0,0,0,0,0,0,0,0
    
    ; 52 R
    .byte %00000000
    .byte %01111100
    .byte %01100110
    .byte %01100110
    .byte %01111100
    .byte %01101100
    .byte %01100110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 53 S
    .byte %00000000
    .byte %00111100
    .byte %01100000
    .byte %00111100
    .byte %00000110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 54 T
    .byte %00000000
    .byte %01111110
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 55 U
    .byte %00000000
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 56 V
    .byte %00000000
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %00111100
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 57 W
    .byte %00000000
    .byte %01000010
    .byte %01000010
    .byte %01011010
    .byte %01111110
    .byte %01100110
    .byte %01100110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 58 X
    .byte %00000000
    .byte %01100110
    .byte %00111100
    .byte %00011000
    .byte %00111100
    .byte %01100110
    .byte %01100110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 59 Y
    .byte %00000000
    .byte %01100110
    .byte %01100110
    .byte %00111100
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 5A Z
    .byte %00000000
    .byte %01111110
    .byte %00000110
    .byte %00001100
    .byte %00011000
    .byte %00110000
    .byte %01111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 5B [
    .byte %00000000
    .byte %00111100
    .byte %00110000
    .byte %00110000
    .byte %00110000
    .byte %00110000
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 5C \
    .byte %00000000
    .byte %00000000
    .byte %01100000
    .byte %00110000
    .byte %00011000
    .byte %00001100
    .byte %00000110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 5D ]
    .byte %00000000
    .byte %01111000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %01111000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 5E ^
    .byte %00000000
    .byte %00011000
    .byte %00111100
    .byte %01100110
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 5F _
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %01111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 60 `
    .byte %00000000
    .byte %00110000
    .byte %00011000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 61 a
    .byte %00000000
    .byte %00000000
    .byte %00111100
    .byte %00000110
    .byte %00111110
    .byte %01100110
    .byte %00111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 62 b
    .byte %00000000
    .byte %00000000
    .byte %01100000
    .byte %01100000
    .byte %01111100
    .byte %01100110
    .byte %01111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 63 c
    .byte %00000000
    .byte %00000000
    .byte %00111110
    .byte %01100000
    .byte %01100000
    .byte %01100000
    .byte %00111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 64 d
    .byte %00000000
    .byte %00000000
    .byte %00000110
    .byte %00000110
    .byte %00111110
    .byte %01100110
    .byte %00111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 65 e
    .byte %00000000
    .byte %00000000
    .byte %00111100
    .byte %01100110
    .byte %01111100
    .byte %01100000
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
     ; 66 f
    .byte %00000000
    .byte %00011100
    .byte %00110110
    .byte %00110000
    .byte %01111000
    .byte %00110000
    .byte %00110000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 67 g
    .byte %00000000
    .byte %00000000
    .byte %00111110
    .byte %01100110
    .byte %00111110
    .byte %00000110
    .byte %01100110
    .byte %00111100
    .byte 0,0,0,0,0,0,0,0
    
    ; 68 h
    .byte %00000000
    .byte %01100000
    .byte %01100000
    .byte %01111000
    .byte %01101100
    .byte %01101100
    .byte %01101100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 69 i
    .byte %00000000
    .byte %00011000
    .byte %00000000
    .byte %00111000
    .byte %00011000
    .byte %00011000
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 6A j
    .byte %00000000
    .byte %00011000
    .byte %00000000
    .byte %00111000
    .byte %00011000
    .byte %01011000
    .byte %00111000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 6B k
    .byte %00000000
    .byte %01100000
    .byte %01100000
    .byte %01101100
    .byte %01111000
    .byte %01101100
    .byte %01101100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 6C l
    .byte %00000000
    .byte %01110000
    .byte %00110000
    .byte %00110000
    .byte %00110000
    .byte %00110000
    .byte %00011100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 6D m
    .byte %00000000
    .byte %00000000
    .byte %01100000
    .byte %01111110
    .byte %01101010
    .byte %01101010
    .byte %01101010
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 6E n
    .byte %00000000
    .byte %00000000
    .byte %01111000
    .byte %01101100
    .byte %01101100
    .byte %01101100
    .byte %01101100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 6F o
    .byte %00000000
    .byte %00000000
    .byte %00111100
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %00111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 70 p
    .byte %00000000
    .byte %00000000
    .byte %01111100
    .byte %01100110
    .byte %01111100
    .byte %01100000
    .byte %01100000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 71 q
    .byte %00000000
    .byte %00000000
    .byte %00111110
    .byte %01100110
    .byte %00111110
    .byte %00000110
    .byte %00000110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 72 r
    .byte %00000000
    .byte %00000000
    .byte %01011100
    .byte %01110110
    .byte %01100000
    .byte %01100000
    .byte %01100000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 73 s
    .byte %00000000
    .byte %00000000
    .byte %00111110
    .byte %01100000
    .byte %00111100
    .byte %00000110
    .byte %01111100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 74 t
    .byte %00000000
    .byte %00011000
    .byte %00011000
    .byte %00111100
    .byte %00011000
    .byte %00011000
    .byte %00001100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 75 u
    .byte %00000000
    .byte %00000000
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %00111010
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 76 v
    .byte %00000000
    .byte %00000000
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %00111100
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 77 w
    .byte %00000000
    .byte %00000000
    .byte %01000010
    .byte %01011010
    .byte %01111110
    .byte %01100110
    .byte %01100110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 78 x
    .byte %00000000
    .byte %00000000
    .byte %01100110
    .byte %00111100
    .byte %00011000
    .byte %00111100
    .byte %01100110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 79 y
    .byte %00000000
    .byte %00000000
    .byte %01100110
    .byte %00111100
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 7A y
    .byte %00000000
    .byte %00000000
    .byte %01111110
    .byte %00001100
    .byte %00011000
    .byte %00110000
    .byte %01111110
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 7B {
    .byte %00000000
    .byte %00011100
    .byte %00110000
    .byte %00110000
    .byte %01100000
    .byte %00110000
    .byte %00011100
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 7C |
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte 0,0,0,0,0,0,0,0
    
    ; 7D }
    .byte %00000000
    .byte %00111000
    .byte %00001100
    .byte %00001100
    .byte %00000110
    .byte %00001100
    .byte %00111000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    ; 7E ~
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte %00110110
    .byte %01101100
    .byte %00000000
    .byte %00000000
    .byte %00000000
    .byte 0,0,0,0,0,0,0,0
    
    .byte %11111111
    .byte %11111111
    .byte %11000011
    .byte %11000011
    .byte %11000011
    .byte %11000011
    .byte %11111111
    .byte %11111111
    .byte 0,0,0,0,0,0,0,0
    
