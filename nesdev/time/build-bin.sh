#!/bin/bash

ca65=$(which 'ca65')
ld65=$(which 'ld65')

$ca65 -g 'time.s' -o 'time.o'
if [ "$?" -ne "0" ] ; then
    exit 1
fi
$ld65 -C 'time-bin.cfg' 'time.o' -o 'time.bin'
if [ "$?" -ne "0" ] ; then
    exit 1
fi
