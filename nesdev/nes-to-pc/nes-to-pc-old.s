;
; iNES header
;

.segment "HEADER"

INES_MAPPER = 0 ; 0 = NROM
INES_MIRROR = 1 ; 0 = horizontal mirroring, 1 = vertical mirroring
INES_SRAM   = 0 ; 1 = battery backed SRAM at $6000-7FFF

.byte 'N', 'E', 'S', $1A ; ID
.byte $02 ; 16k PRG chunk count
.byte $01 ; 8k CHR chunk count
.byte INES_MIRROR | (INES_SRAM << 1) | ((INES_MAPPER & $f) << 4)
.byte (INES_MAPPER & %11110000)
.byte $0, $0, $0, $0, $0, $0, $0, $0 ; padding

.segment "OAM"
oam: .res 256        ; sprite OAM data to be uploaded by DMA

;
; vectors placed at top 6 bytes of memory area
;

.segment "VECTORS"
.word nmi
.word reset
.word irq

;
; reset routine

;
.segment "CODE"
.byte '^', 'T', 'o', 'k', 'i', 'k', 'o', $0
reset:
    SEI
    
    ;ldx #0
    ;:
    ;    lda start, X
    ;    sta $0700, X
    ;    inx
    ;    bne :-
    ;
    ;jmp $0700


start:
    f_counter = $01 

    lda #60
    sta f_counter
    
    ; enable rendering
    lda #%00001000
    sta $2001
    ;waiting for vsync
    :
        bit $2002
        bpl :-
    
    lda #0
    sta $2005
    sta $2005
    
    lda #%00000000
    sta $2000

    ;load pattern table    
    lda #0
    sta $2006
    sta $2006
    sta $02
    sta $03
    :
        lda $02
        sta $2006
        lda $03
        sta $2006
        inc $02
        beq :+
            inc $03
            beq :++
        :
        lda #1
        sta $2007
        
    jmp :--
    :

    @reset_ldx:
    ldx #$3F

    @cycle:
        lda 0
        sta $2001
        ; enable rendering
        lda #%00010000
        ;lda #%11111111
        sta $2001
    

        ;waiting for vsync
        :
            bit $2002
            bpl :-
            
        dec f_counter
        bne @cycle
        
        lda #60
        sta f_counter

        lda #$3F
        sta $2006
        lda #00
        sta $2006

        stx $2007
        
        ;lda #0
        ;sta $5
        
        ;:
        ;    lda #$3F
        ;    sta $2006
        ;    lda $05
        ;    sta $2006
        ;    stx $2007
        ;    inc $05
        ;bne :-
        
        dex

        BEQ @reset_ldx
    bne @cycle
    
    
    
    
    
nmi:
jmp reset
irq:
jmp reset

