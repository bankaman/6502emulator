;
; iNES header
;

.segment "HEADER"

INES_MAPPER = 0 ; 0 = NROM
INES_MIRROR = 1 ; 0 = horizontal mirroring, 1 = vertical mirroring
INES_SRAM   = 0 ; 1 = battery backed SRAM at $6000-7FFF

.byte 'N', 'E', 'S', $1A ; ID
.byte $02 ; 16k PRG chunk count
.byte $00 ; 8k CHR chunk count
.byte INES_MIRROR | (INES_SRAM << 1) | ((INES_MAPPER & $f) << 4)
.byte (INES_MAPPER & %11110000)
.byte $0, $0, $0, $0, $0, $0, $0, $0 ; padding

.segment "OAM"
oam: .res 256        ; sprite OAM data to be uploaded by DMA

.segment "ZEROPAGE"
address:   .res 2
counter:   .res 1
counter2:   .res 1
frame_counter: .res 2
frame_counter_updated: .res 1
word_to_str_addr: .res 2
word_to_str_result: .res 6
word_to_str_stable: .res 1
div_value: .res 2
div_mod: .res 2
div_by: .res 2
scroll_x: .res 1
scroll_y: .res 1


;
; vectors placed at top 6 bytes of memory area
;

.segment "VECTORS"
.word nmi
.word reset
.word irq

;
; reset routine

;

.segment "CODE"
.byte '^', 'T', 'o', 'k', 'i', 'k', 'o', $0
reset:
    sei        ; ignore IRQs
    cld        ; disable decimal mode
    ldx #$40
    stx $4017  ; disable APU frame IRQ
    ldx #$ff
    txs        ; Set up stack
    inx        ; now X = 0
    stx $2000  ; disable NMI
    stx $2001  ; disable rendering
    stx $4010  ; disable DMC IRQs

    ; Optional (omitted):
    ; Set up mapper and jmp to further init code here.

    ; The vblank flag is in an unknown state after reset,
    ; so it is cleared here to make sure that @vblankwait1
    ; does not exit immediately.
    bit $2002

    ; First of two waits for vertical blank to make sure that the
    ; PPU has stabilized
@vblankwait1:  
    bit $2002
    bpl @vblankwait1

    ; We now have about 30,000 cycles to burn before the PPU stabilizes.
    ; One thing we can do with this time is put RAM in a known state.
    ; Here we fill it with $00, which matches what (say) a C compiler
    ; expects for BSS.  Conveniently, X is still 0.
    txa
@clrmem:
    sta $000,x
    sta $100,x
    sta $200,x
    sta $300,x
    sta $400,x
    sta $500,x
    sta $600,x
    sta $700,x
    inx
    bne @clrmem

    ; Other things you can do between vblank waits are set up audio
    ; or set up other mapper registers.
    
    sta frame_counter
    sta frame_counter + 1
    sta scroll_x
    sta scroll_y
    sta word_to_str_result
    
    ;lda #39
    ;sta frame_counter
   
@vblankwait2:
    bit $2002
    bpl @vblankwait2


    
    lda #$11
    ldy #$0
    jsr set_pallete_color
    
    jsr long_wait
    jsr long_wait
    jsr long_wait
    jsr long_wait
    jsr long_wait
    jsr long_wait
    
    
    jsr init
    
    sei
    
@cycle:

    lda frame_counter_updated
    beq :+
        ldx #.LOBYTE(frame_counter)
        stx word_to_str_addr
        ldx #.HIBYTE(frame_counter)
        stx word_to_str_addr + 1
       
        jsr word_to_str
        lda #0
        sta frame_counter_updated
    :
    
    jmp @cycle
    
nmi:
    pha

    inc frame_counter
    bne :+
        inc frame_counter + 1
    :
    
    lda #1
    sta frame_counter_updated
    
    lda word_to_str_stable
    beq :+
    
        ldx #$20
        ldy #$63
        jsr set_ppu_address
        
        lda #.HIBYTE(word_to_str_result)
        sta address+1
        lda #.LOBYTE(word_to_str_result)
        sta address
        jsr print_string
        
    :
    
    jsr set_scroll
    
    pla
    rti

irq:
jmp reset

word_to_str:
    lda #0
    sta word_to_str_result
    sta word_to_str_stable

    ;load value to divide
    ldy #0
    lda (word_to_str_addr),y
    sta div_value
    iny
    lda (word_to_str_addr),y
    sta div_value + 1
    
    lda #10
    sta div_by
    lda #0
    sta div_by + 1

    :
        jsr div_words

        lda div_mod
        adc #48
        pha
        ldy #0
        :
            lda word_to_str_result,y
            tax
            pla
            sta word_to_str_result,y
            iny
            txa
            pha
        bne :-

        pla
        sta word_to_str_result,y

        lda div_value
        ora div_value + 1
    bne :--
    lda #1
    sta word_to_str_stable 
rts
 
div_words:
    
    lda #0
    sta div_mod
    sta div_mod + 1
    clc
    
    ldx #16
    @div_loop:
    
        rol div_value
        rol div_value + 1
        rol div_mod
        rol div_mod + 1
        
        sec 
        lda div_mod
        sbc div_by
        tay 
        lda div_mod + 1
        sbc div_by + 1

        bcc @ignore_result
        
        sty div_mod
        sta div_mod + 1
    
    @ignore_result:
        dex
        bne @div_loop
        
        rol div_value
        rol div_value + 1
        clc
    
rts

long_wait:
    ldx #$FF
    ldy #$FF
@waitloop:
    dex
    bne @waitloop
    dey
    bne @waitloop
rts

disable_render:
    lda #0
    sta $2000
    sta $2001
    rts

enable_render:
    lda #%10000000
    sta $2000
    lda #%00001010
    
    sta $2001
    rts

set_ppu_address:
    bit $2002
    stx $2006
    sty $2006
    rts
    
clear_chr_ram:
    ldx #0
    ldy #0
    jsr set_ppu_address
    
    lda #$1F
    sta counter
    lda #$FF
    sta counter2
    
    ldx #0
    
    @loop:
    stx $2007
    
    dec counter2
    bne @loop
    
    dec counter
    bne @loop
    
    rts
    
load_letters_to_ppu:
    lda #.LOBYTE(letters)
    sta address
    lda #.HIBYTE(letters)
    sta address+1
    
    lda #128
    sta counter
    lda #0
    sta counter2
    
    ldx #0
    ldy #0
    jsr set_ppu_address
    
    @loop:
            lda (address),y
            sta $2007
            
            inc counter2
            lda counter2
            cmp #16
            bne :+
                lda #0
                sta counter2
                dec counter
                beq @end
            :
            inc address
        bne @loop
            inc address+1
        jmp @loop
        
    @end:
            
    rts

set_background_tile:
    ldx #$20
    ldy #0
    jsr set_ppu_address
    sta $2007
    rts
    
set_pallete_color:
    ldx #$3F
    jsr set_ppu_address
    sta $2007
    rts
    
load_pallete:
    ;BG
    lda #$0d
    ldy #0
    jsr set_pallete_color
    
    ;pallete 0
    lda #$30
    ldy #1
    jsr set_pallete_color
    lda #$11
    ldy #2
    jsr set_pallete_color
    lda #$19
    ldy #3
    jsr set_pallete_color
    
    ;pallete 1
    lda #$30
    ldy #5
    jsr set_pallete_color
    lda #$11
    ldy #6
    jsr set_pallete_color
    lda #$19
    ldy #7
    
    ;pallete 2
    lda #$30
    ldy #$9
    jsr set_pallete_color
    lda #$11
    ldy #$A
    jsr set_pallete_color
    lda #$19
    ldy #$B
    jsr set_pallete_color
    
    ;pallete 3
    lda #$30
    ldy #$D
    jsr set_pallete_color
    lda #$11
    ldy #$E
    jsr set_pallete_color
    lda #$19
    ldy #$F
    jsr set_pallete_color
    
    rts

set_scroll:
    ldx scroll_x
    stx $2005
    ldx scroll_y
    stx $2005
    rts
    
clear_screen:
    ldx #$20
    ldy #$00
    jsr set_ppu_address
    :
        lda #$20
        :
            sta $2007
            iny 
            bne :-
            
        inx
        txa
        cmp #$23
        bne :--
    rts
    
print_all_tiles:
    ldx #$20
    ldy #$20
    lda #0
    jsr set_ppu_address
    :
        :
            sta $2007
            adc #1
            beq @end
            iny 
            bne :-
            
        inx
        txa
        cmp #$23
        bne :--
    @end:
    rts

increment_address:
    inc address
    bne @end
        inc address+1
    @end:
    rts
    
print_string:
    :
        ldy #0
        lda (address),y
        beq @end
            
        sta $2007
        jsr increment_address
    jmp :-
    
    @end:
    rts
    

print_hello_world:
    jsr clear_screen
    
    ldx #$20
    ldy #$22
    jsr set_ppu_address
    
    lda #$80
    sta address+1
    lda #0
    sta address
    jsr print_string
    
    ldx #$20
    ldy #$42
    jsr set_ppu_address
    
    lda #.HIBYTE(hello_world_str)
    sta address+1
    lda #.LOBYTE(hello_world_str)
    sta address
    jsr print_string
rts

init:
    ;jsr clear_chr_ram
    jsr load_pallete
    jsr load_letters_to_ppu
    
    jsr print_hello_world

    ;lda #$20
    ;sta $2007  
    ;lda #$20
    ;sta $2007  
    ;lda #$21
    ;sta $2007  
    ;lda #$21
    ;sta $2007  
   
    jsr enable_render
    
    rts

.include "letters.inc"

hello_world_str:
.byte "Hello World!",$0

