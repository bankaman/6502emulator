#!/bin/bash

ca65=$(which 'ca65')
ld65=$(which 'ld65')

$ca65 -g 'nes-to-pc.s' -o 'nes-to-pc.o'
if [ "$?" -ne "0" ] ; then
    exit 1
fi
$ld65 -C 'nes-to-pc-nes.cfg' 'nes-to-pc.o' -o 'nes-to-pc.nes'
if [ "$?" -ne "0" ] ; then
    exit 1
fi
