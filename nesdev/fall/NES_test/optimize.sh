#!/bin/bash

convert to_tiles.png -crop 8x8x8x8 tileset/%04d.png

files=$(find tileset -name '*.png' | sort -h)

files=($files)
signatures=()

for f1 in ${!files[*]} ; do
    echo "sign $f1"
    signatures[$f1]="$(identify -verbose ${files[$f1]} | grep signature)"
done

for f1 in ${!files[*]} ; do
    fname=${files[$f1]}
    if [ ! -f "$fname" ] ; then
        continue
    fi
    sim=()
    for f2 in ${!files[*]} ; do 
        if [ ! -f "${files[$f2]}" ] ; then
            continue
        fi
        if [ "$f1" == "$f2" ]; then
            continue;
        fi
        echo $f1 - $f2
        if [ "${signatures[$f1]}" == "${signatures[$f2]}" ]; then 
            sim+=($f2)
        fi
    done
    
    #echo ${sim[*]}
    
    
    fname="${fname##*/}"
    mv "${files[$f1]}" "optimize/${fname}"
    for i in ${sim[*]} ; do
        file=${files[$i]}
        file="${file##*/}"
        file="${file%.*}"
        echo "$fname" > "optimize/${file}.txt"
        rm ${files[$i]}
    done
done

#identify -verbose kitten.png kitten2.png | grep signature

for t in $(find optimize  -name "*.png") ; do  cp $t unical/${t##*/} ; done
for file in $(find unical  -name "*.png") ; do fname=${file##*/} ; convert $file -depth 8 rgb/${fname%.*}.rgb ; done

