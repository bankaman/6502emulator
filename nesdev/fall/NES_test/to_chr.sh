#!/bin/bash

colors=("000100" "FD0102" "FF5B00" "FF9C00")

files=$(find rgb -name '*.rgb' | sort -h)
files=($files)
rm out.chr
counter=0
for i in ${!files[*]} ; do
    file=${files[$i]}
    dump=($(hexdump $file -v -e '/1 "%02X "'))
    
    color=""
    t=0
    row=()
    f_b=()
    s_b=()
    for k in ${!dump[*]} ; do
        color="$color${dump[$k]}"
        t=$(( $t + 1 ))
        if [ $t -gt 2 ] ; then
            c_num=-1
            for c in ${!colors[*]} ; do
                if [ "${colors[$c]}" == "$color" ]; then
                    c_num=$c
                fi
            done
            if [ "$c_num" -eq "-1" ]; then
                echo "Udnefined color $color, $file"
                exit 1
            fi
            color=""
            t=0
            row+=($c_num)
            if [ ${#row[*]} -gt 7 ]; then
                r1=""
                r2=""
                for v in ${row[*]}; do 
                    if [ $v -eq 0 ]; then
                        r1="${r1}0"
                        r2="${r2}0"
                    fi
                    if [ $v -eq 1 ]; then
                        r1="${r1}0"
                        r2="${r2}1"
                    fi
                    if [ $v -eq 2 ]; then
                        r1="${r1}1"
                        r2="${r2}0"
                    fi
                    if [ $v -eq 3 ]; then
                        r1="${r1}1"
                        r2="${r2}1"
                    fi
                done
                
                #echo "$r1 $r2"
                f_b+=($r1)
                s_b+=($r2)
                row=();
                #exit 0;
            fi
        fi
    done
    #echo ${f_b[*]}
    #echo ${f_b[*]}
    
    for byte in ${f_b[*]} ; do
        h=$((2#$byte))
        h=$(echo "obase=16; $h" | bc)
        echo $h
        echo -n -e "\x$h" >>out.chr
        counter=$(( $counter + 1 ))
        
        #echo "ibase=16; $h" | bc >>out.chr
        
        #echo -n -e "\x$h" >> out.chr
        #printf "\\0b$byte"
    done
    for byte in ${s_b[*]} ; do
        h=$((2#$byte))
        h=$(echo "obase=16; $h" | bc)
        echo $h
        echo -n -e "\x$h" >>out.chr
        counter=$(( $counter + 1 ))
        #echo "ibase=16; $h" | bc >>out.chr
        #echo -n -e "\x$h" >> out.chr
        #printf "\\0b$byte"
    done
    #exit 0
done 
echo $counter
