#!/bin/bash

library=($(find unical -name '*.png' | sort -h))
for i in ${!library[*]}; do 
    library[$i]=${library[$i]##*/}
done
map=($(find optimize -name '*.*' | sort -h))
for i in ${!map[*]}; do 
    map[$i]=${map[$i]##*/}
done

echo -n ".byte "

for element in ${map[*]} ; do
    ext=${element##*.}
    index=-1
    if [ "$ext" == "png" ]; then
        #echo $element
        for i in ${!library[*]}; do
            if [ "${library[$i]}" == "$element" ]; then
                index=$i
            fi
        done
        if [ $index -lt 0 ]; then
            echo "\n element was not found $element"
            exit 1
        fi
        echo -n "$index, "
    else
        el=$(cat "optimize/$element")
        index=-1
        for i in ${!library[*]}; do
            if [ "${library[$i]}" == "$el" ]; then
                index=$i
            fi
        done
        if [ $index -lt 0 ]; then
            echo "\n element was not found $element"
            exit 1
        fi
        echo -n "$index, "
        #exit 0
    fi
done
