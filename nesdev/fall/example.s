;
; iNES header
;

.segment "HEADER"

INES_MAPPER = 0 ; 0 = NROM
INES_MIRROR = 1 ; 0 = horizontal mirroring, 1 = vertical mirroring
INES_SRAM   = 0 ; 1 = battery backed SRAM at $6000-7FFF

.byte 'N', 'E', 'S', $1A ; ID
.byte $02 ; 16k PRG chunk count
.byte $01 ; 8k CHR chunk count
.byte INES_MIRROR | (INES_SRAM << 1) | ((INES_MAPPER & $f) << 4)
.byte (INES_MAPPER & %11110000)
.byte $0, $0, $0, $0, $0, $0, $0, $0 ; padding

;
; CHR ROM
;

.segment "TILES"
.incbin "background.chr"
.incbin "sprite.chr"

;
; vectors placed at top 6 bytes of memory area
;

.segment "VECTORS"
.word nmi
.word reset
.word irq

;
; reset routine
;

.segment "CODE"
reset:
	sei       ; mask interrupts
	lda #0
	sta $2000 ; disable NMI
	sta $2001 ; disable rendering
	sta $4015 ; disable APU sound
	sta $4010 ; disable DMC IRQ
	lda #$40
	sta $4017 ; disable APU IRQ
	cld       ; disable decimal mode
	ldx #$FF
	txs       ; initialize stack
	; wait for first vblank
	bit $2002
	:
		bit $2002
		bpl :-
	; clear all RAM to 0
	lda #0
	ldx #0
	:
		sta $0000, X
		sta $0100, X
		sta $0200, X
		sta $0300, X
		sta $0400, X
		sta $0500, X
		sta $0600, X
		sta $0700, X
		inx
		bne :-
	; place all sprites offscreen at Y=255
	lda #255
	ldx #0
	:
		sta oam, X
		inx
		inx
		inx
		inx
		bne :-
	; wait for second vblank
	:
		bit $2002
		bpl :-
	; NES is initialized, ready to begin!
	; enable the NMI for graphical updates, and jump to our main program
	lda #%10001000
	sta $2000
	jmp main

.segment "OAM"
oam: .res 256        ; sprite OAM data to be uploaded by DMA

.segment "ZEROPAGE"
map_counter:          .res 1 ; address of map
map_addr:             .res 2 ; address of map
nmi_lock:       .res 1 ; prevents NMI re-entry
draw_address:   .res 2
scroll_x: .res 1 
scroll_y: .res 1 

.segment "CODE"
irq:
	rti

; nmi 
nmi:

    pha

    ; prevent NMI re-entry
	lda nmi_lock
	beq :+
		jmp @nmi_end
	:
	lda #1
	sta nmi_lock
	
	
	;scroll
	lda scroll_x
	beq :+
        dec scroll_x
    :
    
    lda scroll_x
    bne :+++ 
        lda scroll_y
        cmp 32
        bpl :+
            inc scroll_y
        :
        
        cmp 0
        bpl :+
            dec scroll_y
        :
	:
	
	lda scroll_x
    sta $2005
    lda scroll_y
    sta $2005
	
	
	
	
	lda #0
	sta nmi_lock
    @nmi_end:
	; restore registers and return

    pla
	rti


main:

    lda #$FF
    sta scroll_x

    lda #0
	and #%00000011 ; keep only lowest 2 bits to prevent error
	ora #%10001000
	sta $2000

    lda #0
    sta $2005
    lda #0
    sta $2005

    ;setting palete
    lda #$0f
    ldx #$3f
    ldy #00
    stx $2006
    sty $2006
    sta $2007
    
    lda #$27
    sta $2007
    lda #$16
    sta $2007
    lda #$28
    sta $2007
    
    ;fill bg
    ldx #$20
    ldy #$00
    stx $2006
    sty $2006
    stx draw_address+1
    sty draw_address
    
    lda #.LOBYTE(the_map)
    ldy #.HIBYTE(the_map)
    sta map_addr
    sty map_addr+1
    
    
    
    ldy #0
    @draw_loop:
        
        ;:
		;bit $2002
		;bpl :-
        
        lda draw_address+1
        sta $2006
        lda draw_address
        sta $2006
        
        inc draw_address
        bne :+
            inc draw_address+1
        :
        
        lda (map_addr),Y 
        sta $2007
        iny
        bne @draw_loop
            inc map_addr+1
            inc map_counter
            lda map_counter
            cmp #4
            bne @draw_loop
           
    ; enable rendering
    lda #%00001000
    sta $2001  
    
@loop:
    jmp @loop
    

.segment "CODE"
the_map:
.byte 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 0, 0, 4, 5, 0, 0, 6, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 9, 0, 0, 10, 11, 0, 0, 12, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 17, 18, 19, 20, 21, 22, 0, 23, 24, 25, 0, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 27, 28, 29, 30, 31, 32, 0, 0, 33, 34, 31, 35, 36, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 38, 31, 39, 40, 31, 41, 0, 0, 0, 42, 31, 43, 31, 44, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 31, 46, 47, 48, 31, 49, 50, 51, 52, 31, 31, 31, 53, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 31, 31, 56, 57, 31, 31, 31, 58, 59, 31, 31, 31, 60, 61, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 31, 31, 63, 31, 31, 31, 31, 31, 60, 60, 64, 64, 64, 65, 66, 67, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 70, 31, 31, 31, 31, 31, 60, 60, 64, 64, 64, 71, 72, 64, 73, 74, 75, 76, 77, 78, 0, 0, 0, 0, 0, 0, 79, 80, 81, 82, 0, 83, 31, 31, 31, 31, 60, 64, 64, 64, 64, 64, 73, 84, 85, 86, 87, 0, 0, 0, 86, 88, 0, 0, 0, 0, 0, 0, 89, 90, 86, 91, 92, 93, 31, 60, 60, 64, 64, 64, 64, 64, 94, 86, 95, 0, 96, 97, 98, 66, 0, 99, 86, 100, 0, 0, 0, 0, 0, 0, 0, 101, 90, 86, 86, 86, 102, 103, 64, 64, 64, 94, 94, 86, 86, 104, 66, 0, 105, 106, 86, 107, 0, 108, 86, 109, 0, 0, 0, 0, 0, 0, 0, 0, 110, 111, 112, 113, 114, 86, 86, 86, 86, 86, 86, 86, 86, 86, 115, 116, 117, 86, 118, 0, 119, 120, 86, 121, 0, 0, 0, 0, 0, 0, 0, 0, 122, 123, 124, 125, 126, 86, 86, 86, 86, 86, 86, 86, 86, 127, 0, 128, 86, 115, 0, 0, 129, 86, 130, 131, 0, 0, 0, 0, 0, 0, 0, 0, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 86, 86, 86, 86, 86, 86, 86, 86, 142, 143, 86, 111, 144, 0, 0, 0, 0, 0, 0, 0, 0, 0, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 86, 86, 86, 86, 86, 86, 111, 156, 157, 86, 158, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 159, 160, 161, 162, 163, 164, 165, 166, 167, 86, 86, 86, 86, 86, 168, 169, 0, 170, 171, 77, 172, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 173, 174, 86, 0, 0, 111, 175, 86, 86, 86, 86, 176, 177, 0, 0, 178, 66, 179, 180, 181, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 182, 183, 184, 185, 186, 187, 188, 86, 86, 86, 168, 189, 0, 190, 86, 86, 86, 86, 86, 191, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 192, 193, 194, 195, 86, 86, 86, 86, 196, 0, 197, 86, 198, 199, 0, 200, 90, 201, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 202, 86, 86, 86, 86, 203, 0, 204, 86, 86, 118, 0, 0, 0, 205, 206, 207, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 208, 86, 86, 86, 130, 0, 0, 209, 86, 86, 0, 0, 0, 0, 210, 86, 211, 212, 213, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 214, 86, 86, 215, 0, 0, 216, 86, 86, 217, 0, 0, 0, 218, 86, 219, 86, 86, 220, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 221, 222, 223, 224, 68, 135, 225, 86, 226, 227, 0, 228, 229, 230, 231, 232, 233, 234, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 235, 236, 237, 238, 239, 240, 241, 86, 242, 243, 244, 86, 245, 246, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 247, 248, 249, 250, 251, 252, 253, 254, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0


