#!/bin/bash

ca65='/home/banka/git/cc65/bin/ca65'
ld65='/home/banka/git/cc65/bin/ld65'

$ca65 -g 'example.s' -o 'example.o'
$ld65 -C 'example.cfg' 'example.o' -o 'example.nes'
