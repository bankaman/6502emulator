#ifndef NCINTERFACE_H
#define NCINTERFACE_H

#include "ncurses.h"
#include <string>

#include "defines.h"

class NCInterface
{
public:
    class TPanel
    {
    public:
        ~TPanel();

        void init(std::string header);

        void resize(int row, int col);
        void move(int row, int col);
        void update();
        void append(std::string data);
        bool selected;

        std::string header;
        std::string buffer;

        //PANEL* panel;
        WINDOW* w;
        WINDOW* window;

    };

    static void run();
    static void resize();
    static void update_cpu_state();
    static void update_ppu_state();
    static void update_memory_view();

    static TPanel debug;
    static TPanel disasm;
    static TPanel cpu_state;
    static TPanel ppu_state;
    static TPanel memory_view;

    static int disasm_address;
    static int memory_address_cpu;
    static int memory_address_ppu;

    static bool memview_cpu;

private:

    static std::string by_bit(unsigned char b);
};

#endif // NCINTERFACE_H
