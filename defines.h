#ifndef DEFINES_H
#define DEFINES_H

#include <vector>
#include <iostream>
#include <sstream>

#include "ncinterface.h"

#define BYTE unsigned char
#define ByteArray std::vector<BYTE>
#define uint unsigned int
#define uint16 unsigned short int

#define memory_access_map_callback(a) void(*a)(uint address, uint size , uint mode, uint &value)

template<typename T>
std::vector<T> slice(std::vector<T> const &v, int m, int n)
{
    auto first = v.cbegin() + m;
    auto last = v.cbegin() + n + 1;

    std::vector<T> vec(first, last);
    return vec;
}

void std_debug();

template <typename Head, typename... Tail>
void std_debug(Head H, Tail... T) {
  std::cerr << H << ' ';
  std_debug(T...);
}

//static std::stringstream debug_stream;

void ndebug(std::string lol);
std::string to_hex(uint val);


#endif // DEFINES_H
