#ifndef DISASM_H
#define DISASM_H

#include "defines.h"

class Disasm
{
public:
    static std::string do_disasm(uint16 address, uint count);

};

#endif // DISASM_H
